const data = [
  {
    name: 'Khoai tây chiên',
    price: '45,000đ x 2 = 90000',
    money: 90000,
  },
  {
    name: 'Bắp bơ chiên hành',
    price: '35,000đ x 2 = 70000',
    money: 70000,
  },
  {
    name: 'Mỳ ý rau củ',
    price: '60,000đ x 1 = 70000',
    money: 60000,
  },
];

export default data;

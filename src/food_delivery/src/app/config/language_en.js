export default {
  // Category screen
  Category_Header: 'CATEGORY',
  // Merchants screen
  Merchants_Header: 'MERCHANTS',
  Merchants_HintInSearchBar: 'Bubble tea, bun dau, banh trang tron ...',
  // Merchants detail screen
  MerchantDetail_Header: 'DETAIL',
  MerchantDetail_DeliveryPrice: 'Delivery price',
  MerchantDetail_Rating: 'Rating',
  MerchantDetail_Prices: 'Prices',
  MerchantDetail_Comment_Send: 'COMMENT',
  MerchantDetail_Comment_PlaceHolder: 'Write your comment here',
  MerchantDetail_Toast_Add: 'A food is added to your basket',
  MerchantDetail_Toast_Remove: 'A food is removed to your basket',
  // Map screen
  Map_Header: 'MAP',
  // Login Screen
  Login_ForgotPassword: 'Forgot Password?',
  Login_Register: 'REGISTER',
  Login_SignIn: 'LOGIN',
  Login_EmailPlaceHolder: 'Username or email',
  Login_PasswordPlaceHolder: 'Password',
  // Register Screen
  Register_Title: 'REGISTER',
  Register_Password: 'Password',
  Register_ConfirmPassword: 'Confirm Password',
  Register_Cancel: 'CANCEL',
  // Forgot password screen
  ForgotPassword_Title: 'FORGOT PASSWORD',
  ForgotPassword_Description: 'Enter the email that you use to sign in to',
  ForgotPassword_SendPassword: 'SEND MY PASSWORD',
  ForgotPassword_Cancel: 'CANCEL',
  // Basket Screen
  Basket_header: 'BASKET',
  Basket_Total: 'Total price',
  Basket_Name: 'Name',
  Basket_Phone: 'Phone',
  Basket_Street: 'Street',
  Basket_District: 'District',
  Basket_Ward: 'Ward',
  Basket_Order: 'Order',
  Basket_Emty: 'Your basket is empty',
  Basket_Info: 'Your infomation',
  Basket_Submit: 'Submit',
  // Order Screen:
  Order_header: 'ORDER',
  Order_detail_header: 'ORDER DETAIL',
  Order_id: 'Order ID',
  Order_Merchant: 'Merchant',
  Order_TotalPrice: 'Total price',
  Order_Date: 'Date',
  Order_Phone: 'Phone',
  Order_Address: 'Address',
  Order_Quanlity: 'Quantity',
  Order_YourOrder: 'Your order',
  // Setting Screen:
  Setting_header: 'SETTING',
  Setting_Phone: 'Phone',
  Setting_Address_District: 'Address - District',
  Setting_Address_Ward: 'Address - Ward',
  Setting_Address_Street: 'Address - Street',
  Setting_Save: 'Save',
  // Notification Scree:
  Notification_header: 'NOTIFICATION',
  // Bottom tab bar
  CategoryTab: 'Category',
  MerchantsTab: 'Merchants',
  NotificationTab: 'Notification',
  OrderTab: 'Order',
  SettingTab: 'Setting',
};

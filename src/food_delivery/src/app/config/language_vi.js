export default {
  // Category screen
  Category_Header: 'PHÂN LOẠI',
  // Merchants screen
  Merchants_Header: 'NHÀ HÀNG',
  Merchants_HintInSearchBar: 'Trà sữa, bún đậu, bánh tráng trộn ...',
  // Merchant detail screen
  MerchantDetail_Header: 'CHI TIẾT',
  MerchantDetail_DeliveryPrice: 'Phí giao hàng',
  MerchantDetail_Rating: 'Đánh giá',
  MerchantDetail_Prices: 'Mức giá',
  MerchantDetail_Comment_Send: 'ĐĂNG',
  MerchantDetail_Comment_PlaceHolder: 'Viết bình luận của bạn ở đây',
  MerchantDetail_Toast_Add: 'Một món vừa được thêm vào giỏ của bạn',
  MerchantDetail_Toast_Remove: 'Một món vừa được lấy ra giỏ của bạn',
  // Map screen
  Map_Header: 'BẢN ĐỒ',
  // Login Screen
  Login_ForgotPassword: 'Quên mật khẩu?',
  Login_Register: 'ĐĂNG KÝ',
  Login_SignIn: 'ĐĂNG NHẬP',
  Login_EmailPlaceHolder: 'Username hoặc email',
  Login_PasswordPlaceHolder: 'Mật khẩu',
  // Register Screen
  Register_Title: 'ĐĂNG KÝ',
  Register_Password: 'Mật khẩu',
  Register_ConfirmPassword: 'Nhập lại mật khẩu',
  Register_Cancel: 'HỦY',
  // Forgot password screen
  ForgotPassword_Title: 'LẤY LẠI MẬT KHẨU',
  ForgotPassword_Description: 'Nhập email mà bạn dùng để đăng nhập',
  ForgotPassword_SendPassword: 'GỞI',
  ForgotPassword_Cancel: 'HỦY',
  // Basket Screen
  Basket_header: 'GIỎ HÀNG',
  Basket_Total: 'Tổng cộng',
  Basket_Name: 'Tên',
  Basket_Phone: 'Số điện thoại',
  Basket_Street: 'Đường',
  Basket_District: 'Quận',
  Basket_Ward: 'Phường',
  Basket_Order: 'Đặt hàng',
  Basket_Emty: 'Giỏ hàng của bạn đang trống',
  Basket_Info: 'Thông tin của bạn',
  Basket_Submit: 'Gởi đơn hàng',
  // Order Screen:
  Order_header: 'ĐƠN HÀNG',
  Order_detail_header: 'CHI TIẾT',
  Order_id: 'ID đơn hàng',
  Order_Merchant: 'Nhà hàng',
  Order_TotalPrice: 'Tổng tiền',
  Order_Date: 'Ngày tạo',
  Order_Phone: 'Số điện thoại',
  Order_Address: 'Địa chỉ',
  Order_Quantity: 'Số lượng',
  Order_YourOrder: 'Món bạn đặt',
  // Setting Screen:
  Setting_header: 'CÀI ĐẶT',
  Setting_Phone: 'Số điện thoại',
  Setting_Address_District: 'Địa chỉ - Quận',
  Setting_Address_Ward: 'Địa chỉ - Phường',
  Setting_Address_Street: 'Địa chỉ - Đường',
  Setting_Save: 'LƯU',
  // Notification Scree:
  Notification_header: 'THÔNG BÁO',
  // Bottom tab bar
  CategoryTab: 'Phân loại',
  MerchantsTab: 'Nhà hàng',
  NotificationTab: 'Thông báo',
  OrderTab: 'Đơn hàng',
  SettingTab: 'Cài đặt',
};

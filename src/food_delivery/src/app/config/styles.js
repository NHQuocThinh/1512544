const myStyles = {
  colors: {
    headerColor: '#37474f',
    primaryColor: '#37474f',
    lightColor: '#fafafa',
    rating: '#4caf50',
    feeShip: '#1e88e5',
    lightColor2: '#eceff1',
  },
  text: {
    fontSize: 14,
    lineHeight: 16,
  },
};

export default myStyles;

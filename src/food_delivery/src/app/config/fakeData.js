import images from './images';

const data = [
  {
    id: 22,
    img: images.food1,
    name: 'Trà sữa Heekcaa Việt Nam - Huỳnh Thúc Kháng',
    address: '305 Đường 3 Tháng 2, P. 10, Quận 10, TP. HCM',
    openTime: '07:00 - 22:00',
    isOpen: true,
    price: '35,000 - 65,000',
    deliveryPrice: '7,500đ/km',
    rating: '8.7',
    distance: '1.4km',
    deliveryTime: '30',
    menu: [
      {
        img: images.menuFood,
        name: 'Súp cua thập cẩm',
        ingredient: 'Trứng cút, óc heo, bắc thảo',
        price: '20000',
      },
      {
        img: images.menuFood,
        name: 'Súp cua (thường)',
        ingredient: '2 trứng cút',
        price: '13000',
      },
      {
        img: images.menuFood,
        name: 'Súp cua đặc biệt',
        ingredient: '',
        price: '32000',
      },
      {
        img: images.menuFood,
        name: 'Súp cua óc heo',
        ingredient: '',
        price: '16000',
      },
      {
        img: images.menuFood,
        name: 'Súp cua bắc thảo',
        ingredient: '',
        price: '16000',
      },
      {
        img: images.menuFood,
        name: 'Súp cua tủy',
        ingredient: '',
        price: '16000',
      },
    ],
  },
  {
    id: 23,
    img: images.food2,
    name: 'Hanuri - Quán Ăn Hàn Quốc - Nguyễn Đình Chiểu',
    address: '14 Bùi Viện, P. Phạm Ngũ Lão, Quận 1, TP. HCM',
    openTime: '06:00 - 22:30',
    isOpen: true,
    price: '35,000 - 55,000',
    deliveryPrice: '7,500đ/km',
    rating: '7.1',
    distance: '3.2km',
    deliveryTime: '40',
    menu: [
      {
        img: images.menuFood,
        name: 'Súp cua thập cẩm',
        ingredient: 'Trứng cút, óc heo, bắc thảo',
        price: '20000',
      },
      {
        img: images.menuFood,
        name: 'Súp cua (thường)',
        ingredient: '2 trứng cút',
        price: '13000',
      },
      {
        img: images.menuFood,
        name: 'Súp cua đặc biệt',
        ingredient: '',
        price: '32000',
      },
      {
        img: images.menuFood,
        name: 'Súp cua óc heo',
        ingredient: '',
        price: '16000',
      },
      {
        img: images.menuFood,
        name: 'Súp cua bắc thảo',
        ingredient: '',
        price: '16000',
      },
      {
        img: images.menuFood,
        name: 'Súp cua tủy',
        ingredient: '',
        price: '16000',
      },
    ],
  },
];

export default data;

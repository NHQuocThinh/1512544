import I18n from 'react-native-i18n';
import en from './language_en';
import vi from './language_vi';

I18n.fallbacks = true;

I18n.translations = {
  en,
  vi,
};

export default I18n;

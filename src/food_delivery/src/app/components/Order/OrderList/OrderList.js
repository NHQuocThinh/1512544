import React from 'react';
import {
  FlatList, TouchableOpacity, Text, View, ActivityIndicator,
} from 'react-native';
import styles from './OrderListStyle';
import myStyles from '../../../config/styles';
import I18n from '../../../config/I18n';

const OrderListItem = ({ data, navigateToOrderDetail }) => {
  const {
    id, totalPrice, phone, address, restaurantName,
  } = data;
  const date = new Date(data.date);
  const parseDate = `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
  return (
    <TouchableOpacity
      style={styles.itemContainer}
      onPress={() => {
        navigateToOrderDetail(data);
      }}
    >
      <View style={styles.itemView1}>
        <Text style={styles.itemField}>{I18n.t('Order_id')}:</Text>
        <Text style={styles.itemContent}>{id}</Text>
      </View>
      <View style={styles.itemView1}>
        <Text style={styles.itemField}>{I18n.t('Order_Merchant')}:</Text>
        <Text style={[styles.itemContent, { width: 240 }]} ellipsizeMode="tail" numberOfLines={1}>
          {restaurantName}
        </Text>
      </View>
      <View style={styles.itemView1}>
        <Text style={styles.itemField}>{I18n.t('Order_TotalPrice')}:</Text>
        <Text style={styles.itemContent}>{totalPrice}đ</Text>
      </View>
      <View style={styles.itemView1}>
        <Text style={styles.itemField}>{I18n.t('Order_Date')}:</Text>
        <Text style={styles.itemContent}>{parseDate}</Text>
      </View>
      <View style={styles.itemView1}>
        <Text style={styles.itemField}>{I18n.t('Order_Phone')}:</Text>
        <Text style={styles.itemContent}>{phone}</Text>
      </View>
      <View style={styles.itemView1}>
        <Text style={styles.itemField}>{I18n.t('Order_Address')}:</Text>
        <Text style={[styles.itemContent, { width: 240 }]} ellipsizeMode="tail" numberOfLines={1}>
          {address}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const Orderlist = (props) => {
  console.log(props.data);
  if (props.loading) {
    return (
      <ActivityIndicator
        style={{
          flex: 1,
          backgroundColor: 'white',
          justifyContent: 'center',
          alignItems: 'center',
          alignSelf: 'center',
          width: '100%',
          height: '100%',
        }}
        size="large"
        color={myStyles.colors.primaryColor}
      />
    );
  }
  return (
    <FlatList
      style={styles.container}
      data={props.data}
      renderItem={({ item }) => (
        <OrderListItem data={item} navigateToOrderDetail={props.navigateToOrderDetail} />
      )}
      refreshing={props.refreshing}
      onRefresh={props.onRefresh}
    />
  );
};

export default Orderlist;

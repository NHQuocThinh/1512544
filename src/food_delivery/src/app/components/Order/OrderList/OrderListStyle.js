import { StyleSheet } from 'react-native';
import myStyles from '../../../config/styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  itemContainer: {
    width: '90%',
    alignSelf: 'center',
    padding: 10,
    marginVertical: 10,
    borderRadius: 3,
    backgroundColor: myStyles.colors.lightColor,
    shadowOpacity: 0,
    shadowOffset: {
      height: 10,
    },
    shadowRadius: 0,
    elevation: 3,
  },
  itemView1: {
    marginVertical: 2,
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemField: {
    fontWeight: 'bold',
    letterSpacing: 2,
    color: myStyles.colors.primaryColor,
    fontSize: 15,
  },
  itemContent: {
    fontSize: 15,
    marginLeft: 5,
  },
});

export default styles;

import React from 'react';
import {
  View, Text, FlatList, Image, ScrollView,
} from 'react-native';
import styles from './OrderDetailStyle';
import I18n from '../../../config/I18n';

const OrderDetailListItem = ({ data }) => {
  console.log();
  const { name, image, price } = data.data[0];
  const totalPrice = price * data.quantity;
  return (
    <View style={styles.view4}>
      <Image source={{ uri: image }} style={styles.itemImage} />
      <View style={styles.view3}>
        <Text
          style={[styles.content, { width: 310, marginLeft: 0 }]}
          ellipsizeMode="tail"
          numberOfLines={2}
        >
          {name}
        </Text>
        <View style={styles.view5}>
          <Text style={styles.field}>{I18n.t('Order_Quantity')}: </Text>
          <Text style={styles.content}>{data.quantity}</Text>
        </View>
        <View style={styles.view5}>
          <Text style={styles.field}>{I18n.t('Order_TotalPrice')}:</Text>
          <Text style={styles.content}>
            {price}x{data.quantity} = {totalPrice}đ
          </Text>
        </View>
      </View>
    </View>
  );
};

const OrderDetail = (props) => {
  console.log();
  const date = new Date(props.data.date);
  const parseDate = `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
  return (
    <ScrollView style={styles.container}>
      <View style={styles.view2}>
        <View style={styles.view1}>
          <Text style={styles.field}>{I18n.t('Order_id')}:</Text>
          <Text style={styles.content}>{props.data.id}</Text>
        </View>
        <View style={styles.view1}>
          <Text style={styles.field}>{I18n.t('Order_Merchant')}:</Text>
          <Text style={[styles.content, { width: 240 }]} ellipsizeMode="tail" numberOfLines={1}>
            {props.data.restaurantName}
          </Text>
        </View>
        <View style={styles.view1}>
          <Text style={styles.field}>{I18n.t('Order_TotalPrice')}:</Text>
          <Text style={styles.content}>{props.data.totalPrice}đ</Text>
        </View>
        <View style={styles.view1}>
          <Text style={styles.field}>{I18n.t('Order_Date')}:</Text>
          <Text style={styles.content}>{parseDate}</Text>
        </View>
        <View style={styles.view1}>
          <Text style={styles.field}>{I18n.t('Order_Phone')}:</Text>
          <Text style={styles.content}>{props.data.phone}</Text>
        </View>
        <View style={styles.view1}>
          <Text style={styles.field}>{I18n.t('Order_Address')}:</Text>
          <Text style={[styles.content, { width: 240 }]} ellipsizeMode="tail" numberOfLines={1}>
            {props.data.address}
          </Text>
        </View>
      </View>
      <View style={[styles.view2, { marginBottom: 30 }]}>
        <Text style={[styles.field, { fontSize: 25, marginBottom: 10 }]}>{I18n.t('Order_YourOrder')}:</Text>
        <FlatList
          data={props.data.details}
          renderItem={({ item }) => <OrderDetailListItem data={item} />}
        />
      </View>
    </ScrollView>
  );
};

export default OrderDetail;

import { StyleSheet } from 'react-native';
import myStyles from '../../../config/styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingVertical: 10,
    paddingBottom: 20,
  },
  view1: {
    flexDirection: 'row',
    marginVertical: 5,
  },
  view5: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  view4: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  view2: {
    alignSelf: 'center',
    padding: 10,
    width: '90%',
    borderRadius: 3,
    marginVertical: 10,
    backgroundColor: myStyles.colors.lightColor,
    shadowOpacity: 0,
    shadowOffset: {
      height: 10,
    },
    shadowRadius: 0,
    elevation: 3,
  },
  view3: {
    marginLeft: 20,
  },
  field: {
    fontSize: 15,
    fontWeight: 'bold',
    letterSpacing: 3,
    color: myStyles.colors.primaryColor,
  },
  content: {
    fontSize: 15,
    marginLeft: 5,
    color: 'black',
  },
  itemImage: {
    width: 50,
    height: 50,
    borderRadius: 3,
  },
});

export default styles;

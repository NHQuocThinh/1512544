import React from 'react';
import { View } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import styles from './MapStyle';

const Map = props => (
    <View style={styles.container}>
      <MapView
        provider={props.provider}
        style={styles.map}
        initialRegion={props.region}
        onRegionChange={(region) => {
          props.changeRegionDebounce(region.latitude, region.longitude);
        }}
      >
        {props.markers
          && props.markers.map(item => (
            <Marker
              key={item.coordinate}
              coordinate={item.coordinate}
              title={item.RESTAURANT.name}
              onPress={() => {
                const data = item.RESTAURANT;
                props.navigateBackToListView(data);
              }}
            />
          ))}
        {props.currentLocation && (
          <MapView.Marker
            coordinate={{
              latitude: props.currentLocation.latitude,
              longitude: props.currentLocation.longitude,
            }}
          >
            <View style={styles.radius}>
              <View style={styles.marker} />
            </View>
          </MapView.Marker>
        )}
      </MapView>
    </View>
);

export default Map;

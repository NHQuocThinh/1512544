import React from 'react';
import {
  View,
  FlatList,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import styles from './CommentStyle';
import myStyles from '../../../config/styles';
import images from '../../../config/images';
import I18n from '../../../config/I18n';

const CommentItem = ({ data }) => {
  console.log();
  const date = new Date(data.createAt);
  const dateParse = `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
  return (
    <View style={styles.CommentItem_Container}>
      <Image source={images.user} style={{ width: 50, height: 50 }} />
      <View style={styles.CommentItem_View1}>
        <Text style={styles.CommentItem_Name}>{data.name}</Text>
        <Text style={styles.CommentItem_Date}>{dateParse}</Text>
        <Text style={styles.CommentItem_Content}>{data.content === '' ? '--' : data.content}</Text>
      </View>
    </View>
  );
};

const Comment = (props) => {
  console.log();
  if (props.loading) {
    return (
      <ActivityIndicator
        style={{ flex: 1, backgroundColor: 'white' }}
        size="large"
        color={myStyles.colors.primaryColor}
      />
    );
  }
  return (
    <View style={styles.container}>
      <FlatList
        data={props.data}
        renderItem={({ item }) => <CommentItem data={item} />}
        onRefresh={props.reloadComment}
        refreshing={props.refreshing}
      />
      <View style={styles.view1}>
        <TextInput
          style={styles.input}
          placeholder={I18n.t('MerchantDetail_Comment_PlaceHolder')}
          value={props.comment}
          onChangeText={(text) => {
            props.onChangeComment(text);
          }}
        />
        <TouchableOpacity
          onPress={() => {
            props.onSubmit();
          }}
        >
          <Text style={styles.send}>{I18n.t('MerchantDetail_Comment_Send')}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Comment;

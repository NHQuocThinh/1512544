import { StyleSheet } from 'react-native';
import myStyles from '../../../config/styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 20,
  },
  CommentItem_Container: {
    width: '90%',
    alignSelf: 'center',
    flexDirection: 'row',
    marginVertical: 5,
    paddingLeft: 20,
    backgroundColor: myStyles.colors.lightColor,
    shadowOpacity: 0,
    shadowOffset: {
      height: 10,
    },
    shadowRadius: 0,
    elevation: 3,
    borderRadius: 3,
    padding: 10,
  },
  CommentItem_View1: {
    flexDirection: 'column',
    paddingLeft: 20,
  },
  CommentItem_Name: {
    fontSize: 15,
    fontWeight: 'bold',
    letterSpacing: 3,
    color: myStyles.colors.primaryColor,
  },
  CommentItem_Content: {
    marginTop: 5,
    fontSize: 12,
    fontWeight: 'normal',
    width: '90%',
  },
  CommentItem_Date: {
    fontSize: 10,
    fontWeight: 'normal',
    fontStyle: 'italic',
  },
  view1: {
    width: '100%',
    alignSelf: 'center',
    padding: 15,
    backgroundColor: myStyles.colors.primaryColor,
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    padding: 0,
    paddingLeft: 5,
    marginLeft: 10,
    width: '80%',
    marginRight: 20,
    backgroundColor: 'white',
    borderRadius: 3,
  },
  send: {
    marginRight: 20,
    fontWeight: 'bold',
    letterSpacing: 2,
    color: 'white',
    fontSize: 15,
  },
});

export default styles;

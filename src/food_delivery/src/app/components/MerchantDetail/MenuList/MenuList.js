import React from 'react';
import { FlatList, View, ActivityIndicator } from 'react-native';
import styles from './MenuListStyle';
import myStyles from '../../../config/styles';
import MenuListItem from '../../../containers/MerchantDetail/MenuListItem';

const MenuList = (props) => {
  if (props.loading) {
    return (
      <ActivityIndicator
        style={{ flex: 1, backgroundColor: 'white' }}
        size="large"
        color={myStyles.colors.primaryColor}
      />
    );
  }
  return (
    <View style={styles.container}>
      <FlatList
        data={props.data}
        renderItem={({ item }) => <MenuListItem data={item} id={props.id} />}
      />
    </View>
  );
};

export default MenuList;

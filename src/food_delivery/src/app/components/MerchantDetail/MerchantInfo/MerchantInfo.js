import React from 'react';
import {
  View, Text, Image, TouchableOpacity, WebView, FlatList,
} from 'react-native';
import styles from './MerchantInfoStyle';
import I18n from '../../../config/I18n';
import images from '../../../config/images';

const ImageAndVideoItem = ({ data }) => {
  console.log();
  if (data.type === 'image') {
    return (
      <Image
        source={{
          uri: data.url,
        }}
        style={styles.imageContainer}
      />
    );
  }
  return (
    <WebView
      source={{
        uri: data.url,
      }}
      style={styles.imageContainer}
    />
  );
};

const MerchantInfo = (props) => {
  console.log();
  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <FlatList
          data={props.imageAndVideo}
          horizontal={true}
          pagingEnabled={true}
          renderItem={({ item }) => <ImageAndVideoItem data={item} />}
        />
      </View>
      <Text style={[styles.merchantName, { marginLeft: 10 }]}>{props.data.name}</Text>
      <View style={styles.view3}>
        <View style={styles.view4}>
          <Text style={styles.textLabel}>{I18n.t('MerchantDetail_DeliveryPrice')}</Text>
          <Text style={styles.textContent}>
            {props.data.feeShip == null ? '--' : `${props.data.feeShip}đ/km`}
          </Text>
        </View>
        <View style={[styles.view4, { alignItems: 'center' }]}>
          <Text style={styles.textLabel}>{I18n.t('MerchantDetail_Rating')}</Text>
          <View style={styles.view5}>
            {props.data.rating >= 1 && <Image source={images.star} style={styles.star} />}
            {props.data.rating >= 2 && <Image source={images.star} style={styles.star} />}
            {props.data.rating >= 3 && <Image source={images.star} style={styles.star} />}
            {props.data.rating >= 4 && <Image source={images.star} style={styles.star} />}
            {props.data.rating === 5 && <Image source={images.star} style={styles.star} />}
          </View>
        </View>
        {props.isViewComment ? (
          <TouchableOpacity
            style={styles.view6}
            onPress={() => {
              props.viewComment();
            }}
          >
            <Image source={images.menu} style={{ width: 30, height: 30 }} />
            <Text style={[styles.textLabel, { marginLeft: 5, letterSpacing: 0 }]} />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            style={styles.view6}
            onPress={() => {
              props.viewComment();
            }}
          >
            <Image source={images.comment} style={{ width: 30, height: 30 }} />
            <Text style={[styles.textLabel, { marginLeft: 5, letterSpacing: 0 }]}>
              {props.commentNumb}
            </Text>
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

export default MerchantInfo;

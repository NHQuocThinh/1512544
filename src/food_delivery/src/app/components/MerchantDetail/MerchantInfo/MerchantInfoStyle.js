import { StyleSheet, Dimensions } from 'react-native';
import myStyles from '../../../config/styles';

const styles = StyleSheet.create({
  container: {},
  imageContainer: {
    width: Dimensions.get('window').width,
    height: 170,
  },
  view1: {
    width: '100%',
    height: 150,
    backgroundColor: 'rgba(52, 52, 52, 0.4)',
    position: 'absolute',
  },
  view2: {
    position: 'absolute',
    bottom: 10,
    left: 10,
  },
  merchantName: {
    color: myStyles.colors.primaryColor,
    fontSize: 18,
    fontWeight: 'bold',
    marginVertical: 10,
  },
  merchantAddress: {
    fontSize: 8,
    color: myStyles.colors.primaryColor,
  },
  textLabel: {
    fontSize: 14,
    lineHeight: 16,
    color: myStyles.colors.primaryColor,
    fontWeight: 'bold',
    letterSpacing: 3,
    marginVertical: 5,
  },
  textContent: {
    fontSize: 14,
    marginTop: 5,
    color: myStyles.colors.feeShip,
  },
  view3: {
    flexDirection: 'row',
  },
  view4: {
    marginLeft: 10,
    marginRight: 40,
  },
  view5: {
    flexDirection: 'row',
  },
  star: {
    width: 15,
    height: 15,
    marginHorizontal: 2,
  },
  view6: {
    flexDirection: 'row',
    position: 'absolute',
    alignItems: 'center',
    right: 40,
    top: 10,
  },
});

export default styles;

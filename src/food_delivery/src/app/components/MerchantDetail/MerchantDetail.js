import React from 'react';
import { View } from 'react-native';
import styles from './MerchantDetailStyle';
import MerchantInfo from '../../containers/MerchantDetail/MerchantInfo';
import MenuList from '../../containers/MerchantDetail/MenuList';
import Comment from '../../containers/MerchantDetail/Comment';

const Merchants = props => (
  <View style={styles.container}>
    <MerchantInfo data={props.merchantInfo} />
    {!props.isViewComment && <MenuList id={props.id} />}
    {props.isViewComment && <Comment data={props.comments} idRestaurant={props.id}/>}
  </View>
);

export default Merchants;

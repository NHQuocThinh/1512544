import React from 'react';
import {
  View, Image, Text, TouchableOpacity,
} from 'react-native';
import images from '../../../config/images';
import styles from './MenuListItemStyle';
import myStyles from '../../../config/styles';

const MenuListItem = props => (
  <View style={styles.container}>
    <Image source={{ uri: props.data.image }} style={styles.imageContainer} resizeMode="cover" />
    <View style={styles.view1}>
      <Text
        style={[
          styles.text,
          {
            fontWeight: 'bold',
            color: myStyles.colors.primaryColor,
            width: 200,
          },
        ]}
        ellipsizeMode="tail"
        numberOfLines={5}
      >
        {props.data.name}
      </Text>
      <Text style={[styles.text]}>{props.data.price}đ</Text>
    </View>
    <View style={{ flexDirection: 'row', position: 'absolute', right: 20 }}>
      {props.quanlity !== 0 && (
        <TouchableOpacity
          onPress={() => {
            if (props.quanlity >= 1) props.updateQuality(props.quanlity - 1);
            props.removeFoodFromYourOrder(props.data);
          }}
        >
          <Image source={images.subtract} style={{ width: 20, height: 20 }} />
        </TouchableOpacity>
      )}
      {props.quanlity !== 0 && <Text style={styles.quanlity}> {props.quanlity} </Text>}
      <TouchableOpacity
        onPress={() => {
          props.updateQuality(props.quanlity + 1);
          props.addNewFoodToYourOrder(props.data);
        }}
      >
        <Image source={images.add} style={{ width: 20, height: 20 }} />
      </TouchableOpacity>
    </View>
  </View>
);

export default MenuListItem;

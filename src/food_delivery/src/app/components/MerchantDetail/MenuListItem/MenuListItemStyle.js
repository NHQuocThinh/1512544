import { StyleSheet } from 'react-native';
import myStyle from '../../../config/styles';

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 3,
    width: '95%',
    backgroundColor: myStyle.colors.lightColor,
    marginVertical: 5,
    shadowOpacity: 0,
    shadowOffset: {
      height: 10,
    },
    shadowRadius: 0,
    elevation: 3,
  },
  imageContainer: {
    width: 60,
    height: 60,
    borderRadius: 3,
  },
  view1: {
    marginLeft: 20,
  },
  text: {
    fontSize: 14,
    lineHeight: 16,
    marginTop: 5,
    width: 250,
  },
  quanlity: {
    fontWeight: 'bold',
    color: myStyle.colors.primaryColor,
    fontSize: 15,
    marginHorizontal: 2,
  },
});

export default styles;

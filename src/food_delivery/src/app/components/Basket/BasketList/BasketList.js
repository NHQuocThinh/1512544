import React from 'react';
import { FlatList, View } from 'react-native';
import BasketItem from '../../../containers/Basket/BasketListItem';

const BasketList = (props) => {
  console.log();
  return (
    <View style={{ backgroundColor: 'white' }}>
      <FlatList
        data={props.data}
        renderItem={({ item }) => (
          <BasketItem
            data={item}
          />
        )}
      />
    </View>
  );
};

export default BasketList;

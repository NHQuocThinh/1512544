import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
  },
  view1: {
    marginVertical: 10,
  },
  field: {
    fontSize: 20,
    fontWeight: 'bold',
    letterSpacing: 2,
  },
  input: {
    marginTop: 5,
    borderBottomWidth: 0.5,
    padding: 0,
  },
  picker: {
    marginBottom: -10,
  },
});

export default styles;

import React from 'react';
import {
  View, TextInput, Picker, Text,
} from 'react-native';
import styles from './InfoStyle';
import I18n from '../../../config/I18n';

const Info = (props) => {
  console.log();
  const {
    username,
    phone,
    street,
    districtData,
    wardData,
    currentDistrictId,
    currentWardId,
  } = props.orderReducer;
  const districtDataPicker = districtData.map(item => (
    <Picker.Item label={item.name} value={item.id} key={item.id} />
  ));
  const wardDataPicker = wardData.map(item => (
    <Picker.Item label={item.name} value={item.id} key={item.id} />
  ));
  return (
    <View style={styles.container}>
      <View style={styles.view1}>
        <Text style={styles.field}>{I18n.t('Basket_Name')}</Text>
        <TextInput
          style={styles.input}
          value={username}
          onChangeText={(data) => {
            props.updateUsernameOrder(data);
          }}
        />
      </View>
      <View style={styles.view1}>
        <Text style={styles.field}>{I18n.t('Basket_Phone')}</Text>
        <TextInput
          style={styles.input}
          value={phone}
          onChangeText={(data) => {
            props.updatePhoneOrder(data);
          }}
        />
      </View>
      <View style={styles.view1}>
        <Text style={styles.field}>{I18n.t('Basket_Street')}</Text>
        <TextInput
          style={styles.input}
          value={street}
          onChangeText={(data) => {
            props.updateStreetOrder(data);
          }}
        />
      </View>
      <View style={styles.view1}>
        <Text style={styles.field}>{I18n.t('Basket_District')}</Text>
        <Picker
          style={styles.picker}
          selectedValue={currentDistrictId}
          onValueChange={(itemValue) => {
            props.updateCurrentDistrictIdOrder(itemValue);
            props.getWardFromDistrictId(itemValue);
          }}
        >
          {districtDataPicker}
        </Picker>
      </View>
      <View style={styles.view1}>
        <Text style={styles.field}>{I18n.t('Basket_Ward')}</Text>
        <Picker
          style={styles.picker}
          selectedValue={currentWardId}
          onValueChange={(itemValue) => {
            props.updateCurrentWardIdOrder(itemValue);
          }}
        >
          {wardDataPicker}
        </Picker>
      </View>
    </View>
  );
};

export default Info;

import { StyleSheet } from 'react-native';
import myStyle from '../../../config/styles';

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageContainer: {
    width: 20,
    height: 20,
  },
  view1: {
    marginLeft: 10,
  },
  view2: {
    position: 'absolute',
    right: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 15,
    marginTop: 5,
    color: 'black',
    fontWeight: '300',
  },
  quanlity: {
    fontWeight: 'bold',
    color: myStyle.colors.primaryColor,
    fontSize: 15,
    marginHorizontal: 5,
  },
});

export default styles;

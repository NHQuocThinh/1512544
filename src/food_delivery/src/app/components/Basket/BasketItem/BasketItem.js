import React from 'react';
import {
  View, Text, Image, TouchableOpacity,
} from 'react-native';
import styles from './style';
import images from '../../../config/images';
import myStyle from '../../../config/styles';

const BasketListItem = (props) => {
  console.log();
  const totalPrice = props.data.price * props.data.quantity;
  return (
    <View style={styles.container}>
      <View style={styles.view1}>
        <Text
          style={[
            styles.text,
            { fontWeight: 'bold', color: myStyle.colors.primaryColor, width: 300 },
          ]}
          ellipsizeMode="tail"
          numberOfLines={2}
        >
          {props.data.name}
        </Text>
        <Text style={styles.text}>
          {props.data.price} x {props.data.quantity} = {totalPrice}đ
        </Text>
      </View>
      <View style={styles.view2}>
        <TouchableOpacity
          onPress={() => {
            props.removeFoodFromYourOrder(props.data);
          }}
        >
          <Image source={images.subtract} style={styles.imageContainer} />
        </TouchableOpacity>
        <Text style={styles.quanlity}>
          {props.data.quantity}
        </Text>
        <TouchableOpacity
          onPress={() => {
            props.addNewFoodToYourOrder(props.data);
          }}
        >
          <Image source={images.add} style={styles.imageContainer} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default BasketListItem;

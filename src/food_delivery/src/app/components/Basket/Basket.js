import React from 'react';
import {
  View, ScrollView, Text, TouchableHighlight,
} from 'react-native';
import styles from './BasketStyle';
import myStyles from '../../config/styles';
import BasketList from '../../containers/Basket/BasketList';
import I18n from '../../config/I18n';
import Info from '../../containers/Basket/Info';

const calculateTotalPrice = (data) => {
  const totalPrice = data.reduce((price, item) => (price += item.price * item.quantity), 0);
  return totalPrice;
};

const Basket = (props) => {
  console.log();
  const totalPrice = calculateTotalPrice(props.orderData);
  if (props.orderData.length === 0) {
    return (
      <View style={[styles.container, { alignItems: 'center' }]}>
        <Text style={styles.emtyMes}>{I18n.t('Basket_Emty')}</Text>
      </View>
    );
  }
  return (
    <ScrollView style={styles.container}>
      <BasketList />
      <View style={styles.line} />
      <View style={styles.view1}>
        <Text style={styles.totalPrice1}>{I18n.t('Basket_Total')}</Text>
        <Text style={styles.totalPrice2}>{totalPrice}đ</Text>
      </View>
      <View style={styles.view2}>
        <Text style={styles.yourInfomation}>{I18n.t('Basket_Info')}</Text>
        <Info />
      </View>
      <TouchableHighlight
        style={styles.submitContainer}
        underlayColor={myStyles.colors.lightColor}
        onPress={() => {
          props.submitOrder();
        }}
      >
        <Text style={styles.submit}>{I18n.t('Basket_Submit')}</Text>
      </TouchableHighlight>
    </ScrollView>
  );
};

export default Basket;

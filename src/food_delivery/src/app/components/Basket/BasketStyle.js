import { StyleSheet } from 'react-native';
import myStyles from '../../config/styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  emtyMes: {
    marginTop: 30,
    fontSize: 20,
    fontWeight: 'bold',
    letterSpacing: 4,
  },
  line: {
    width: '90%',
    height: 1,
    backgroundColor: myStyles.colors.primaryColor,
    marginTop: 10,
    alignSelf: 'center',
  },
  view1: {
    padding: 20,
    flexDirection: 'row',
  },
  totalPrice1: {
    fontSize: 20,
    fontWeight: '100',
    letterSpacing: 2,
  },
  totalPrice2: {
    fontSize: 20,
    fontWeight: 'bold',
    letterSpacing: 2,
    color: '#c62828',
    paddingHorizontal: 10,
    marginLeft: 10,
  },
  view2: {
    paddingHorizontal: 20,
    paddingVertical: 5,
  },
  yourInfomation: {
    fontSize: 30,
    fontWeight: 'bold',
    letterSpacing: 4,
    color: myStyles.colors.primaryColor,
  },
  submitContainer: {
    alignSelf: 'center',
    width: '90%',
    backgroundColor: myStyles.colors.primaryColor,
    marginVertical: 10,
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3,
    shadowOpacity: 0,
    shadowOffset: {
      height: 10,
    },
    shadowRadius: 0,
    elevation: 3,
  },
  submit: {
    fontSize: 20,
    fontWeight: 'bold',
    letterSpacing: 2,
    color: 'white',
  },
  // view1: {
  //   marginTop: 20,
  //   width: '100%',
  //   flexDirection: 'row',
  //   justifyContent: 'flex-end',
  //   alignItems: 'center',
  // },
  // text: {
  //   fontSize: 15,
  //   letterSpacing: 1.08,
  //   color: '#283593',
  //   fontWeight: 'bold',
  // },
  // priceText: {
  //   margin: 10,
  //   marginRight: 10,
  //   fontSize: 15,
  // },
  // orderText: {
  //   color: 'white',
  //   fontWeight: 'bold',
  //   fontSize: 14,
  //   lineHeight: 16,
  //   letterSpacing: 1.08,
  //   padding: 18,
  // },
  // orderContainer: {
  //   marginTop: 50,
  //   backgroundColor: '#283593',
  //   marginHorizontal: 10,
  //   borderRadius: 3,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  // },
});

export default styles;

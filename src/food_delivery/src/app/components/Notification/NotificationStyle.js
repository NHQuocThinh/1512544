import { StyleSheet } from 'react-native';
import myStyles from '../../config/styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  notiItem_Container: {
    width: '95%',
    borderRadius: 3,
    backgroundColor: myStyles.colors.lightColor,
    shadowOpacity: 0,
    shadowOffset: {
      height: 10,
    },
    shadowRadius: 0,
    elevation: 3,
    alignSelf: 'center',
    marginVertical: 5,
    padding: 10,
  },
  notiItem_View1: {
    flexDirection: 'row',
  },
  notiItem_Title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: myStyles.colors.primaryColor,
    marginBottom: 5,
  },
  notiItem_Content: {
    width: '100%',
  },
});

export default styles;

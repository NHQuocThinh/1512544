import React from 'react';
import {
  View, FlatList, Text, ActivityIndicator,
} from 'react-native';
import styles from './NotificationStyle';
import myStyles from '../../config/styles';

const NotificationItem = ({ data }) => {
  console.log();
  return (
    <View style={styles.notiItem_Container}>
      <View style={styles.notiItem_View1}>
        <Text style={styles.notiItem_Title}>{data.title}</Text>
      </View>
      <View style={styles.notiItem_View1}>
        <Text style={styles.notiItem_Content}>{data.content}</Text>
      </View>
    </View>
  );
};

const Notification = (props) => {
  console.log();
  if (props.loading) {
    return (
      <ActivityIndicator
        style={{ flex: 1, backgroundColor: 'white' }}
        size="large"
        color={myStyles.colors.primaryColor}
      />
    );
  }
  return (
    <View style={styles.container}>
      <FlatList
        data={props.data}
        renderItem={({ item }) => <NotificationItem data={item} />}
        refreshing={props.refreshing}
        onRefresh={props.onRefresh}
      />
    </View>
  );
};

export default Notification;

import React from 'react';
import {
  View, Image, Text, TextInput, ScrollView, TouchableHighlight, Picker,
} from 'react-native';
import styles from './SettingStyle';
import I18n from '../../config/I18n';
import images from '../../config/images';
import myStyles from '../../config/styles';

const Setting = (props) => {
  const {
    districtData, currentDistrictId, currentWardId, wardData,
  } = props.districtAndWardReducer;
  const {
    userName, phone, email, avatarUrl, address,
  } = props.data;
  const url = props.avatar === null
    ? `http://${avatarUrl}?random_number=${new Date().getTime()}`
    : props.avatar;
  const districtDataPicker = districtData.map(item => (
    <Picker.Item label={item.name} value={item.id} key={item.id} />
  ));
  const wardDataPicker = wardData.map(item => (
    <Picker.Item label={item.name} value={item.id} key={item.id} />
  ));
  return (
    <View style={styles.container}>
      <ScrollView style={{ width: '100%' }}>
        <View style={styles.view1}>
          <Image source={{ uri: url }} style={styles.image} />
          <TouchableHighlight
            style={styles.view5}
            underlayColor={myStyles.colors.lightColor}
            onPress={() => {
              props.changeAvatar();
            }}
          >
            <Image source={images.camera} style={styles.camera} />
          </TouchableHighlight>
        </View>
        <View style={styles.view2}>
          <Text style={styles.field}>Username</Text>
          <TextInput
            style={styles.input}
            placeholder={userName}
            onChangeText={(data) => {
              props.updateUsername(data);
            }}
          />
        </View>
        <View style={styles.view2}>
          <Text style={styles.field}>{I18n.t('Setting_Phone')}</Text>
          <TextInput
            style={styles.input}
            placeholder={phone}
            onChangeText={(data) => {
              props.updatePhone(data);
            }}
          />
        </View>
        <View style={styles.view2}>
          <Text style={styles.field}>Email</Text>
          <TextInput style={styles.input} editable={false} value={email} />
        </View>
        <View style={styles.view2}>
          <Text style={styles.field}>{I18n.t('Setting_Address_District')}</Text>
          <Picker
            style={styles.picker}
            selectedValue={currentDistrictId}
            onValueChange={(itemValue) => {
              props.selectDistrict(itemValue);
              props.getWardByDistrict(itemValue);
            }}
          >
            {districtDataPicker}
          </Picker>
        </View>
        <View style={styles.view2}>
          <Text style={styles.field}>{I18n.t('Setting_Address_Ward')}</Text>
          <Picker
            style={styles.picker}
            selectedValue={currentWardId}
            onValueChange={(itemValue) => {
              props.selectWard(itemValue);
            }}
          >
            {wardDataPicker}
          </Picker>
        </View>
        <View style={styles.view2}>
          <Text style={styles.field}>{I18n.t('Setting_Address_Street')}</Text>
          <TextInput
            style={styles.input}
            placeholder={address.street}
            onChangeText={(data) => {
              props.updateAddress(data);
            }}
          />
        </View>
        <TouchableHighlight
          style={styles.saveContainer}
          underlayColor={myStyles.colors.lightColor}
          onPress={() => {
            props.edit();
            props.resetEdited();
          }}
        >
          <Text style={styles.save}>{I18n.t('Setting_Save')}</Text>
        </TouchableHighlight>
      </ScrollView>
    </View>
  );
};

export default Setting;

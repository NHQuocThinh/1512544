import { StyleSheet } from 'react-native';
import myStyles from '../../config/styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  view1: {
    height: 200,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
  },
  view2: {
    width: '90%',
    marginVertical: 10,
    alignSelf: 'center',
  },
  view3: { flex: 1 },
  view5: {
    padding: 10,
    borderRadius: 100,
    position: 'absolute',
    right: 90,
    backgroundColor: myStyles.colors.lightColor,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: 'white',
  },
  image: {
    position: 'absolute',
    width: 170,
    height: 170,
    borderRadius: 100,
  },
  camera: {
    width: 20,
    height: 20,
  },
  field: {
    fontSize: 20,
    fontWeight: 'bold',
    letterSpacing: 2,
    color: myStyles.colors.primaryColor,
  },
  input: {
    marginTop: 5,
    borderBottomWidth: 0.5,
    padding: 0,
  },
  picker: {
    padding: 0,
  },
  saveContainer: {
    alignSelf: 'center',
    width: '90%',
    backgroundColor: myStyles.colors.primaryColor,
    marginTop: 30,
    marginBottom: 10,
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3,
    shadowOpacity: 0,
    shadowOffset: {
      height: 10,
    },
    shadowRadius: 0,
    elevation: 3,
  },
  save: {
    fontSize: 20,
    fontWeight: 'bold',
    letterSpacing: 2,
    color: 'white',
  },
});

export default styles;

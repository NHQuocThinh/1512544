import React from 'react';
import { View, TextInput, Image } from 'react-native';
import images from '../../../config/images';
import I18n from '../../../config/I18n';
import styles from './SearchBarStyle';

const SearchBar = props => (
  <View style={styles.container}>
    <View style={styles.view}>
      <Image source={images.search} style={{ width: 17, height: 17 }} />
      <TextInput
        style={styles.textInput}
        placeholder={I18n.t('Merchants_HintInSearchBar')}
        onChangeText={(text) => {
          props.onSearchRestaurant(text);
        }}
      />
    </View>
  </View>
);

export default SearchBar;

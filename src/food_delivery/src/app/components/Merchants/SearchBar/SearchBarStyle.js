import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  view: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 3,
    marginHorizontal: 10,
    marginVertical: 5,
    paddingHorizontal: 5,
  },
  textInput: {
    backgroundColor: 'white',
    padding: 0,
    marginLeft: 5,
    width: 340,
  },
});

export default styles;

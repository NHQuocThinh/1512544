import React from 'react';
import { View } from 'react-native';
import styles from './MerchantsStyle';
import MerchantsList from '../../containers/Merchants/MerchantsList';
import SearchBar from '../../containers/Merchants/SearchBar';

const Merchants = () => (
  <View style={styles.container}>
    <SearchBar/>
    <MerchantsList />
  </View>
);

export default Merchants;

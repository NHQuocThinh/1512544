import React from 'react';
import {
  View, FlatList, TouchableOpacity, Image, Text, ActivityIndicator,
} from 'react-native';
import styles from './MerchantsListStyle';
import images from '../../../config/images';
import myStyles from '../../../config/styles';

const MerchantItem = ({
  name, image, data, navigate, feeShip, rating,
}) => {
  console.log();
  return (
    <TouchableOpacity
      style={styles.MerchantItem_container}
      onPress={() => {
        navigate(data);
      }}
    >
      <Image
        source={{ uri: image }}
        style={styles.MerchantItem_imageContainer}
        resizeMode="cover"
      />
      <View style={styles.MerchantItem_view1}>
        <Text style={[styles.text, { fontSize: 15 }]} ellipsizeMode="tail" numberOfLines={1}>
          {name}
        </Text>
        <View style={styles.View1}>
          <Text style={styles.rating}>Rating:</Text>
          {rating >= 1 && <Image source={images.star} style={styles.star} />}
          {rating >= 2 && <Image source={images.star} style={styles.star} />}
          {rating >= 3 && <Image source={images.star} style={styles.star} />}
          {rating >= 4 && <Image source={images.star} style={styles.star} />}
          {rating === 5 && <Image source={images.star} style={styles.star} />}
        </View>
        <Text
          style={[
            styles.text,
            { fontWeight: 'normal', fontStyle: 'italic', color: myStyles.colors.feeShip },
          ]}
        >
          FeeShip: {feeShip === null ? '--' : `${feeShip}đ/km`}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const MerchantsList = (props) => {
  console.log();
  if (props.loading) {
    return (
      <ActivityIndicator
        style={{ flex: 1, backgroundColor: 'white' }}
        size="large"
        color={myStyles.colors.primaryColor}
      />
    );
  }
  return (
    <View>
      <FlatList
        style={{ marginBottom: 50 }}
        data={props.data}
        renderItem={({ item }) => (
          <MerchantItem
            data={item.info}
            name={item.info.name}
            image={item.info.image}
            navigate={props.navigateToDetail}
            feeShip={item.info.feeShip}
            rating={item.info.rating}
          />
        )}
        keyExtractor={(item, index) => index}
        onEndReachedThreshold={0.5}
        onEndReached={props.loadMore}
        onRefresh={props.refreshData}
        refreshing={props.refreshing}
      />
    </View>
  );
};

export default MerchantsList;

import { StyleSheet } from 'react-native';
import myStyles from '../../../config/styles';

const styles = StyleSheet.create({
  MerchantItem_container: {
    paddingVertical: 10,
    paddingHorizontal: 10,
    flexDirection: 'row',
    width: '95%',
    borderRadius: 3,
    backgroundColor: myStyles.colors.lightColor,
    shadowOpacity: 0,
    shadowOffset: {
      height: 10,
    },
    shadowRadius: 0,
    elevation: 3,
    alignSelf: 'center',
    marginVertical: 5,
  },
  MerchantItem_imageContainer: {
    width: 80,
    height: 80,
    borderRadius: 3,
  },
  MerchantItem_view1: {
    marginLeft: 20,
  },
  text: {
    fontSize: 13,
    width: 270,
    color: myStyles.colors.primaryColor,
    fontWeight: 'bold',
  },
  rating: {
    fontSize: 13,
    fontStyle: 'italic',
  },
  View1: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 5,
  },
  star: {
    width: 15,
    height: 15,
    marginLeft: 5,
  },
});

export default styles;

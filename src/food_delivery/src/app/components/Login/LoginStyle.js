import { StyleSheet } from 'react-native';
import myStyles from '../../config/styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  forgotPasswordContainer: {
    marginTop: 20,
    marginHorizontal: 20,
  },
  forgotPasswordText: {
    color: myStyles.colors.primaryColor,
  },
  signInContainer: {
    marginTop: 20,
    marginHorizontal: 20,
  },
  signInText: {
    color: myStyles.colors.primaryColor,
  },
});

export default styles;

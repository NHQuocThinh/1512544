import React from 'react';
import {
  View, Image, TouchableOpacity, Text,
} from 'react-native';
import styles from './LoginStyle';
import myStyles from '../../config/styles';
import images from '../../config/images';
import I18n from '../../config/I18n';
import LoginForm from '../../containers/Login/LoginForm';

const Login = (props) => {
  console.log();
  return (
    <View style={styles.container}>
      <View
        style={{
          height: '50%',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Image source={images.login} resizeMode="center" />
      </View>
      <LoginForm authenSuccess={props.authenSuccess} authen={props.authen} />
      <View style={{ alignItems: 'center' }}>
        <TouchableOpacity
          style={styles.forgotPasswordContainer}
          onPress={() => {
            props.navigate('ForgotPassword');
          }}
        >
          <Text style={[myStyles.text, styles.forgotPasswordText]}>
            {I18n.t('Login_ForgotPassword')}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.signInContainer}
          onPress={() => {
            props.navigate('Register');
          }}
        >
          <Text style={[myStyles.text, styles.signInText]}>{I18n.t('Login_Register')}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Login;

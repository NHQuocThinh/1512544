import { StyleSheet } from 'react-native';
import myStyles from '../../../config/styles';

const styles = StyleSheet.create({
  container: {},
  view1: {
    marginHorizontal: 20,
    borderRadius: 3,
  },
  textInput: {
    color: myStyles.colors.primaryColor,
    fontWeight: 'normal',
    fontSize: 14,
    lineHeight: 16,
    borderRadius: 3,
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: myStyles.colors.primaryColor,
    paddingHorizontal: 10,
    paddingVertical: 0,
  },
  loginContainer: {
    height: 50,
    marginTop: 50,
    backgroundColor: myStyles.colors.primaryColor,
    marginHorizontal: 20,
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center',
    shadowOpacity: 0,
    shadowOffset: {
      height: 10,
    },
    shadowRadius: 0,
    elevation: 3,
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
    lineHeight: 16,
    letterSpacing: 1.08,
    padding: 18,
  },
  errorMsgText: {
    marginLeft: 20,
    marginTop: 20,
    color: 'red',
    fontWeight: 'bold',
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 1.08,
  },
});

export default styles;

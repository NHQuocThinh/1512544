import React from 'react';
import {
  View, Text, TextInput, TouchableOpacity, ActivityIndicator,
} from 'react-native';
import styles from './LoginFormStyle';
import I18n from '../../../config/I18n';

const LoginForm = (props) => {
  console.log();
  return (
    <View style={styles.container}>
      <View style={styles.view1}>
        <TextInput
          style={styles.textInput}
          placeholder={I18n.t('Login_EmailPlaceHolder')}
          placeholderTextColor="#CCCFCE"
          value={props.email}
          autoCapitalize="none"
          keyboardType="email-address"
          onChangeText={(data) => {
            props.onChangeEmail(data);
          }}
        />
      </View>
      <View style={[styles.view1, { marginTop: 20 }]}>
        <TextInput
          style={styles.textInput}
          placeholder={I18n.t('Login_PasswordPlaceHolder')}
          placeholderTextColor="#CCCFCE"
          secureTextEntry={true}
          autoCapitalize="none"
          value={props.password}
          onChangeText={(data) => {
            props.onChangePassword(data);
          }}
        />
      </View>
      <Text
        style={
          props.errorMsg === 'nothing'
            ? [styles.errorMsgText, { color: 'white' }]
            : styles.errorMsgText
        }
      >
        {props.errorMsg}
      </Text>
      <TouchableOpacity
        style={styles.loginContainer}
        onPress={props.onPressLogin}
        disabled={props.loading}
      >
        {props.loading ? (
          <ActivityIndicator animating={props.loading} color="white" style="small" />
        ) : (
          <Text style={styles.loginText}>{I18n.t('Login_SignIn')}</Text>
        )}
      </TouchableOpacity>
    </View>
  );
};

export default LoginForm;

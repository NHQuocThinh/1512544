import React from 'react';
import {
  View, Text, TextInput, TouchableOpacity, ActivityIndicator,
} from 'react-native';
import { withNavigation } from 'react-navigation';
import styles from './RegisterFormStyle';
import myStyle from '../../../../config/styles';
import I18n from '../../../../config/I18n';

const RegisterForm = (props) => {
  console.log();
  return (
    <View style={styles.container}>
      <View style={styles.view1}>
        <Text style={[myStyle.text, styles.label]}>Email</Text>
        <TextInput
          style={[styles.textInput, myStyle.text]}
          keyboardType="email-address"
          autoCapitalize="none"
          value={props.email}
          onChangeText={(data) => {
            props.onChangeEmail(data);
          }}
        />
      </View>
      <View style={styles.view1}>
        <Text style={[myStyle.text, styles.label]}>{I18n.t('Register_Password')}</Text>
        <TextInput
          style={[styles.textInput, myStyle.text]}
          secureTextEntry={true}
          autoCapitalize="none"
          value={props.password}
          onChangeText={(data) => {
            props.onChangePassword(data);
          }}
        />
      </View>
      <View style={styles.view1}>
        <Text style={[myStyle.text, styles.label]}>{I18n.t('Register_ConfirmPassword')}</Text>
        <TextInput
          style={[styles.textInput, myStyle.text]}
          secureTextEntry={true}
          autoCapitalize="none"
          value={props.rePassword}
          onChangeText={(data) => {
            props.onChangeRePassword(data);
          }}
        />
      </View>
      <Text
        style={
          props.errorMsg === 'nothing'
            ? [styles.errorMsgText, { color: 'white' }]
            : styles.errorMsgText
        }
      >
        {props.errorMsg}
      </Text>
      <TouchableOpacity
        style={styles.buttonContainer}
        onPress={() => {
          props.onPressRegister();
        }}
      >
        {props.loading ? (
          <ActivityIndicator animating={props.loading} style="small" color="white" />
        ) : (
          <Text style={styles.buttonText}>{I18n.t('Register_Title')}</Text>
        )}
      </TouchableOpacity>
      <TouchableOpacity
        style={[styles.buttonContainer, { marginTop: 10 }]}
        onPress={() => props.navigateBackToLogin()}
      >
        <Text style={styles.buttonText}>{I18n.t('Register_Cancel')}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default withNavigation(RegisterForm);

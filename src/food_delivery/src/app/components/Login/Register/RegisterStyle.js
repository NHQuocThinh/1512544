import { StyleSheet } from 'react-native';
import myStyles from '../../../config/styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  view1: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  line: {
    height: 1,
    width: '20%',
    marginHorizontal: 30,
    backgroundColor: myStyles.colors.primaryColor,
  },
  registerText: {
    fontSize: 35,
    color: myStyles.colors.primaryColor,
  },
});

export default styles;

import React from 'react';
import { View, Text } from 'react-native';
import styles from './RegisterStyle';
import I18n from '../../../config/I18n';
import RegisterForm from '../../../containers/Login/RegisterForm';

const Register = (props) => {
  console.log();
  return (
    <View style={styles.container}>
      <View style={styles.view1}>
        <View style={styles.line} />
        <Text style={styles.registerText}>{I18n.t('Register_Title')}</Text>
        <View style={styles.line} />
      </View>
      <RegisterForm authenSuccess={props.authenSuccess} authen={props.authen} />
    </View>
  );
};

export default Register;

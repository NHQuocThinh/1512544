import { StyleSheet } from 'react-native';
import myStyles from '../../../config/styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  view1: {
    marginTop: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  line: {
    height: 1,
    width: '20%',
    marginHorizontal: 30,
    backgroundColor: myStyles.colors.primaryColor,
  },
  ForgotPasswordText: {
    fontSize: 30,
    color: myStyles.colors.primaryColor,
    fontWeight: 'bold',
    letterSpacing: 1.08,
  },
  announceText: {
    marginTop: 5,
    fontWeight: 'normal',
    color: myStyles.colors.lightColor,
  },
  imageContainer: {
    height: '40%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;

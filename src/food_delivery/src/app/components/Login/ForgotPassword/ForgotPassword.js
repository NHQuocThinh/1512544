import React from 'react';
import { View, Text, Image } from 'react-native';
import myStyle from '../../../config/styles';
import styles from './ForgotPasswordStyle';
import images from '../../../config/images';
import I18n from '../../../config/I18n';
import ForgotPasswordForm from '../../../containers/Login/ForgotPasswordForm';

const ForgotPassword = () => {
  console.log();
  return (
    <View style={styles.container}>
      <View style={styles.view1}>
        <Text style={styles.ForgotPasswordText}>{I18n.t('ForgotPassword_Title')}</Text>
        <Text style={[myStyle.text, styles.announceText]}>
          {I18n.t('ForgotPassword_Description')}
        </Text>
      </View>
      <View style={styles.imageContainer}>
        <Image source={images.forgotPassword} resizeMode="center" />
      </View>
      <ForgotPasswordForm />
    </View>
  );
};

export default ForgotPassword;

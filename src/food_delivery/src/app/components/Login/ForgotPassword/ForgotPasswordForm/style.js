import { StyleSheet } from 'react-native';
import myStyles from '../../../../config/styles';

const styles = StyleSheet.create({
  container: {},
  view1: {
    marginHorizontal: 20,
  },
  label: {
    fontSize: 16,
    fontWeight: 'bold',
    color: myStyles.colors.primaryColor,
  },
  textInput: {
    color: myStyles.colors.primaryColor,
    fontWeight: 'normal',
    borderRadius: 3,
    width: '100%',
    paddingVertical: 0,
    paddingHorizontal: 0,
    borderBottomColor: myStyles.colors.primaryColor,
    backgroundColor: 'white',
    borderBottomWidth: 1,
    marginTop: 2,
  },
  buttonContainer: {
    height: 50,
    marginTop: 100,
    backgroundColor: myStyles.colors.primaryColor,
    marginHorizontal: 20,
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 1.08,
    padding: 18,
  },
  errorMsgText: {
    marginLeft: 20,
    marginTop: 20,
    color: 'red',
    fontWeight: 'bold',
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 1.08,
  },
});

export default styles;

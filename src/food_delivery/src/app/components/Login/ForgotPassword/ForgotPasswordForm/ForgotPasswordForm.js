import React from 'react';
import {
  View, TextInput, Text, TouchableOpacity, ActivityIndicator,
} from 'react-native';
import styles from './style';
import myStyle from '../../../../config/styles';
import I18n from '../../../../config/I18n';

const ForgotPasswordForm = (props) => {
  console.log();
  return (
    <View style={styles.container}>
      <View style={styles.view1}>
        <Text style={[styles.label, myStyle.text]}>Email</Text>
        <TextInput
          style={styles.textInput}
          keyboardType="email-address"
          autoCapitalize="none"
          value={props.email}
          onChangeText={(data) => {
            props.onChangeEmail(data);
          }}
        />
      </View>
      <Text
        style={
          props.errorMsg === 'nothing'
            ? [styles.errorMsgText, { color: 'white' }]
            : styles.errorMsgText
        }
      >
        {props.errorMsg}
      </Text>
      <TouchableOpacity style={styles.buttonContainer} onPress={props.onPressSendMyPassword}>
        {props.loading ? (
          <ActivityIndicator animating={props.loading} style="small" color="white" />
        ) : (
          <Text style={styles.buttonText}>{I18n.t('ForgotPassword_SendPassword')}</Text>
        )}
      </TouchableOpacity>
      <TouchableOpacity
        style={[styles.buttonContainer, { marginTop: 10 }]}
        onPress={() => {
          props.navigate();
        }}
      >
        <Text style={styles.buttonText}>{I18n.t('ForgotPassword_Cancel')}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ForgotPasswordForm;

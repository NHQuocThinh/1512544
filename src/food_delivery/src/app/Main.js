import React from 'react';
import {
  Image, StyleSheet, View, TouchableOpacity, Text,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import Login from './containers/Login/Login';
import Register from './containers/Login/Register';
import ForgotPassword from './containers/Login/ForgetPassword';
import Merchants from './containers/Merchants/Merchants';
import MerchantDetail from './containers/MerchantDetail/MerchantDetail';
import Basket from './containers/Basket/Basket';
import Order from './containers/Order/Order';
import OrderDetail from './containers/Order/OrderDetail';
import MerchantsMap from './containers/Map/Map';
import Notification from './containers/Notification/Notification';
import Setting from './containers/Setting/Setting';
import images from './config/images';
import I18n from './config/I18n';
import myStyles from './config/styles';

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 60,
    backgroundColor: myStyles.colors.headerColor,
    paddingHorizontal: 10,
    shadowOpacity: 0,
    shadowOffset: {
      height: 10,
    },
    shadowRadius: 0,
    elevation: 10,
  },
  headerTitle: {
    marginLeft: 20,
    fontSize: 20,
    fontWeight: 'bold',
    letterSpacing: 1.1,
    color: '#fff',
    width: 150,
  },
  imageContainer: { width: 20, height: 20 },
  indicatorContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
});

const MerchantStack = createStackNavigator(
  {
    Merchants_MerchantStack: {
      screen: Merchants,
      navigationOptions: ({ navigation }) => ({
        header: (
          <View style={styles.headerContainer}>
            <Text style={styles.headerTitle}>{I18n.t('Merchants_Header')}</Text>
            <TouchableOpacity
              onPress={() => {
                const { routeName } = navigation.state;
                if (routeName === 'Merchants_MerchantStack') navigation.navigate('Basket_MerchantStack');
                else if (routeName === 'Merchants_CategoryStack') navigation.navigate('Basket_CategoryStack');
              }}
            >
              <Icon name="shopping-basket" size={20} color="white" style={{ marginLeft: 150 }} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                const { routeName } = navigation.state;
                if (routeName === 'Merchants_MerchantStack') navigation.navigate('Map_MerchantStack');
                else if (routeName === 'Merchants_CategoryStack') navigation.navigate('Map_CategoryStack');
              }}
            >
              <Icon name="map" size={20} color="white" style={{ marginLeft: 20 }} />
            </TouchableOpacity>
          </View>
        ),
      }),
    },
    MerchantDetail_MerchantStack: {
      screen: MerchantDetail,
      navigationOptions: ({ navigation }) => ({
        header: (
          <View style={styles.headerContainer}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}
            >
              <Icon name="long-arrow-left" size={20} color="white" style={{ marginLeft: 5 }} />
            </TouchableOpacity>
            <Text style={styles.headerTitle}>{I18n.t('MerchantDetail_Header')}</Text>
          </View>
        ),
      }),
    },
    Basket_MerchantStack: {
      screen: Basket,
      navigationOptions: ({ navigation }) => ({
        header: (
          <View style={styles.headerContainer}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}
            >
              <Icon name="long-arrow-left" size={20} color="white" style={{ marginLeft: 5 }} />
            </TouchableOpacity>
            <Text style={styles.headerTitle}>{I18n.t('Basket_header')}</Text>
          </View>
        ),
      }),
    },
    Map_MerchantStack: {
      screen: MerchantsMap,
      navigationOptions: ({ navigation }) => ({
        header: (
          <View style={styles.headerContainer}>
            <Text style={[styles.headerTitle, { width: 100 }]}>{I18n.t('Map_Header')}</Text>
            <TouchableOpacity
              onPress={() => {
                const { routeName } = navigation.state;
                if (routeName === 'Map_MerchantStack') navigation.navigate('Basket_MerchantStack');
                else if (routeName === 'Map_CategoryStack') navigation.navigate('Basket_CategoryStack');
              }}
            >
              <Icon name="shopping-basket" size={20} color="white" style={{ marginLeft: 200 }} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}
            >
              <Icon name="list" size={20} color="white" style={{ marginLeft: 20 }} />
            </TouchableOpacity>
          </View>
        ),
      }),
    },
  },
  {
    initialRouteName: 'Merchants_MerchantStack',
  },
);

const NotificationStack = createStackNavigator(
  {
    Notification: {
      screen: Notification,
      navigationOptions: ({ navigation }) => ({
        header: (
          <View style={styles.headerContainer}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}
            />
            <Text style={styles.headerTitle}>{I18n.t('Notification_header')}</Text>
          </View>
        ),
      }),
    },
  },
  {
    initialRouteName: 'Notification',
  },
);

const SettingStack = createStackNavigator(
  {
    Setting: {
      screen: Setting,
      navigationOptions: ({ navigation }) => ({
        header: (
          <View style={styles.headerContainer}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}
            />
            <Text style={styles.headerTitle}>{I18n.t('Setting_header')}</Text>
          </View>
        ),
      }),
    },
  },
  {
    initialRouteName: 'Setting',
  },
);

const OrderStack = createStackNavigator(
  {
    Order: {
      screen: Order,
      navigationOptions: ({ navigation }) => ({
        header: (
          <View style={styles.headerContainer}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}
            />
            <Text style={styles.headerTitle}>{I18n.t('Order_header')}</Text>
          </View>
        ),
      }),
    },
    OrderDetail: {
      screen: OrderDetail,
      navigationOptions: ({ navigation }) => ({
        header: (
          <View style={styles.headerContainer}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}
            >
              <Icon name="long-arrow-left" size={20} color="white" style={{ marginLeft: 5 }} />
            </TouchableOpacity>
            <Text style={styles.headerTitle}>{I18n.t('Order_detail_header')}</Text>
          </View>
        ),
      }),
    },
  },
  {
    initialRouteName: 'Order',
  },
);

const BottomTabNavigator = createBottomTabNavigator({
  Merchants: {
    screen: MerchantStack,
    navigationOptions: {
      title: I18n.t('MerchantsTab'),
      tabBarIcon: () => {
        const iconName = images.merchantIcon;
        return <Image source={iconName} style={{ width: 20, height: 20 }} />;
      },
      tabBarOptions: {
        labelStyle: {
          fontWeight: 'bold',
          letterSpacing: 2,
          fontSize: 12,
        },
      },
    },
  },
  Order: {
    screen: OrderStack,
    navigationOptions: {
      title: I18n.t('OrderTab'),
      tabBarIcon: () => {
        const iconName = images.orderIcon;
        return <Image source={iconName} style={{ width: 20, height: 20 }} />;
      },
      tabBarOptions: {
        labelStyle: {
          fontWeight: 'bold',
          letterSpacing: 2,
          fontSize: 12,
        },
      },
    },
  },
  Notification: {
    screen: NotificationStack,
    navigationOptions: {
      title: I18n.t('NotificationTab'),
      tabBarIcon: () => {
        const iconName = images.notificationIcon;
        return <Image source={iconName} style={{ width: 20, height: 20 }} />;
      },
      tabBarOptions: {
        labelStyle: {
          fontWeight: 'bold',
          letterSpacing: 2,
          fontSize: 12,
        },
      },
    },
  },
  Setting: {
    screen: SettingStack,
    navigationOptions: {
      title: I18n.t('SettingTab'),
      tabBarIcon: () => {
        const iconName = images.settingIcon;
        return <Image source={iconName} style={{ width: 20, height: 20 }} />;
      },
      tabBarOptions: {
        labelStyle: {
          fontWeight: 'bold',
          letterSpacing: 2,
          fontSize: 12,
        },
      },
    },
  },
});

const MainStack = createStackNavigator(
  {
    Login: {
      screen: Login,
      navigationOptions: () => ({
        header: null,
      }),
    },
    Register: {
      screen: Register,
      navigationOptions: () => ({
        header: null,
      }),
    },
    ForgotPassword: {
      screen: ForgotPassword,
      navigationOptions: () => ({
        header: null,
      }),
    },
    BottomTabNavigator: {
      screen: BottomTabNavigator,
    },
  },
  {
    initialRouteName: 'Login',
    headerMode: 'none',
  },
);

export default MainStack;

import React from 'react';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import axios from 'axios';
import SearchBar from '../../components/Merchants/SearchBar/SearchBar';
import { searchingRestaurant, loadRestaurantData, updateCurrentPage } from '../../actions';
import { API } from '../../config/API';

class SearchBarContainer extends React.Component {
  onSearchRestaurant = async (data) => {
    if (data === '') {
      this.props.updateCurrentPage(1);
      this.props.loadData(1);
    } else {
      this.props.searchingRestaurant(data);
    }
  };

  render() {
    return <SearchBar onSearchRestaurant={this.onSearchRestaurant} />;
  }
}

const mapDispatchToProps = dispatch => ({
  updateCurrentPage: data => dispatch(updateCurrentPage(data)),
  loadData: async (page) => {
    const url = `${API}restaurant/getAll/10&${page}`;
    try {
      const response = await axios({
        url,
        method: 'GET',
      });
      if (response.status === 200) {
        return dispatch(loadRestaurantData(response.data.data));
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  },
  searchingRestaurant: async (data) => {
    const url = `${API}restaurant/search?name=${data}`;
    try {
      const response = await axios({
        url,
        method: 'GET',
      });
      if (response.status === 200) {
        const result = response.data.map(item => ({
          info: item,
          address: {},
        }));
        return dispatch(searchingRestaurant(result));
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  },
});

export default withNavigation(connect(mapDispatchToProps)(SearchBarContainer));

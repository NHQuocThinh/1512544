import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { API } from '../../config/API';
import Merchants from '../../components/Merchants/Merchants';
import {
  loadRestaurantData,
  loadCategoryData,
} from '../../actions';

class MerchantsContainer extends React.Component {
  render() {
    return <Merchants />;
  }
}

const mapDispatchToProps = dispatch => ({
  getCategoryDataFromServer: async () => {
    const url = `${API}categories/getAll`;
    try {
      const response = await axios({
        url,
        method: 'GET',
      });
      if (response.status === 200) {
        return dispatch(loadCategoryData(response.data));
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  },
  getAllRestaurantDataByCategoryFromServer: async (categoryId) => {
    const url = `${API}restaurant/getCategory/${categoryId}`;
    try {
      const response = await axios({
        url,
        method: 'GET',
      });
      if (response.status === 200) {
        const result = response.data.map(item => ({
          info: item,
          address: {},
        }));
        return dispatch(loadRestaurantData(result));
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  },
});

export default connect(
  mapDispatchToProps,
)(MerchantsContainer);

import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { withNavigation } from 'react-navigation';
import MerchantsList from '../../components/Merchants/MerchantsList/MerchantsList';
import { API } from '../../config/API';
import { loadRestaurantData, updateCurrentPage } from '../../actions';

class MerchantsListContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      refreshing: false,
    };
  }

  componentDidMount() {
    this.props.loadData(1).then(() => {
      this.setState({ loading: false });
    });
  }

  navigateToDetail = (input) => {
    this.props.navigation.navigate('MerchantDetail_MerchantStack', {
      data: input,
    });
  };

  loadMore = () => {
    const page = this.props.currentPage + 1;
    this.props.updateCurrentPage(page);
    this.props.loadMoreData(this.props.data, page);
  };

  refreshData = () => {
    this.setState({ refreshing: true });
    this.props.loadData(1);
    this.props.updateCurrentPage(1);
    this.setState({ refreshing: false });
  };

  render() {
    return (
      <MerchantsList
        data={this.props.data}
        loadMore={this.loadMore}
        loading={this.state.loading}
        refreshData={this.refreshData}
        refreshing={this.state.refreshing}
        navigateToDetail={this.navigateToDetail}
      />
    );
  }
}

const mapStateToProps = state => ({
  data: state.RestaurantReducer.data,
  currentPage: state.RestaurantReducer.currentPage,
});

const mapDispatchToProps = dispatch => ({
  loadData: async (page) => {
    const url = `${API}restaurant/getAll/10&${page}`;
    try {
      const response = await axios({
        url,
        method: 'GET',
      });
      if (response.status === 200) {
        return dispatch(loadRestaurantData(response.data.data));
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  },
  loadMoreData: async (data, page) => {
    const url = `${API}restaurant/getAll/10&${page}`;
    try {
      const response = await axios({
        url,
        method: 'GET',
      });
      if (response.status === 200) {
        if (response.data.data.length === 0) {
          return dispatch(updateCurrentPage(page - 1));
        }
        let result = data;
        result = result.concat(response.data.data);
        return dispatch(loadRestaurantData(result));
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  },
  updateCurrentPage: data => dispatch(updateCurrentPage(data)),
});

export default withNavigation(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(MerchantsListContainer),
);

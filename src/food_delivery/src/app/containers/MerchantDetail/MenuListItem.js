import React from 'react';
import { ToastAndroid } from 'react-native';
import { connect } from 'react-redux';
import MenuListItem from '../../components/MerchantDetail/MenuListItem/MenuListItem';
import { addNewFood, removeFood, updateRestaurantId } from '../../actions';
import I18n from '../../config/I18n';

class MenuListItemContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      quanlity: 0,
    };
  }

  addNewFoodToYourOrder = (data) => {
    const { restaurantId } = this.props.orderReducer;
    if (restaurantId !== data.idRestaurant) {
      this.props.updateRestaurantId(data.idRestaurant);
    }
    let added = false;
    const oldOrderData = this.props.orderReducer.data;
    const newItem = {
      idFood: data.id,
      price: data.price,
      name: data.name,
      quantity: 1,
    };
    const newOrderData = oldOrderData.map((item) => {
      if (item.idFood === newItem.idFood) {
        added = true;
        return { ...item, quantity: item.quantity + 1 };
      }
      return item;
    });
    if (added === false) {
      newOrderData.push(newItem);
    }
    this.props.addNewFood(newOrderData);
    ToastAndroid.show(I18n.t('MerchantDetail_Toast_Add'), ToastAndroid.SHORT);
  };

  removeFoodFromYourOrder = (data) => {
    let removedIndex = -1;
    const oldOrderData = this.props.orderReducer.data;
    const removedItem = {
      idFood: data.id,
    };
    const newOrderData = oldOrderData.map((item, index) => {
      if (item.idFood === removedItem.idFood) {
        removed = true;
        if (item.quantity - 1 === 0) removedIndex = index;
        return { ...item, quantity: item.quantity - 1 };
      }
      return item;
    });
    if (removedIndex !== -1) {
      newOrderData.splice(removedIndex, 1);
    }
    this.props.removeFood(newOrderData);
    ToastAndroid.show(I18n.t('MerchantDetail_Toast_Remove'), ToastAndroid.SHORT);
  };

  updateQuality = (data) => {
    this.setState({ quanlity: data });
  };

  render() {
    return (
      <MenuListItem
        data={this.props.data}
        quanlity={this.state.quanlity}
        addNewFoodToYourOrder={this.addNewFoodToYourOrder}
        removeFoodFromYourOrder={this.removeFoodFromYourOrder}
        updateQuality={this.updateQuality}
      />
    );
  }
}

const mapStateToProps = state => ({
  orderReducer: state.OrderReducer,
});

const mapDispatchToProps = dispatch => ({
  addNewFood: data => dispatch(addNewFood(data)),
  removeFood: data => dispatch(removeFood(data)),
  updateRestaurantId: data => dispatch(updateRestaurantId(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MenuListItemContainer);

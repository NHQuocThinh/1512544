import React from 'react';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import { resetOrder, viewComment } from '../../actions';
import MerchantDetail from '../../components/MerchantDetail/MerchantDetail';

class MerchantDetailContainer extends React.Component {
  componentDidMount() {
    if (this.props.merchantDetailReducer.viewComment) this.props.viewComment();
  }

  render() {
    return (
      <MerchantDetail
        merchantInfo={this.props.navigation.state.params.data}
        id={this.props.navigation.state.params.data.id}
        isViewComment={this.props.merchantDetailReducer.viewComment}
        comments={this.props.merchantDetailReducer.comments}
      />
    );
  }
}

const mapStateToProps = (state) => {
  console.log();
  return {
    merchantDetailReducer: state.MerchantDetailReducer,
  };
};

const mapDispatchToProps = dispatch => ({
  resetOrder: () => dispatch(resetOrder()),
  viewComment: () => dispatch(viewComment()),
});

export default withNavigation(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(MerchantDetailContainer),
);

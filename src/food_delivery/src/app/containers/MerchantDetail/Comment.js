import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { API } from '../../config/API';
import { loadComment } from '../../actions/index';
import Comment from '../../components/MerchantDetail/Comment/Comment';

class CommentContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      comment: '',
      refreshing: false,
      loading: false,
    };
  }

  onChangeComment = (data) => {
    this.setState({ comment: data });
  };

  onSubmit = async () => {
    if (this.state.comment !== '') {
      this.setState({ loading: true });
      const data = {
        name: this.props.accountReducer.data.userName,
        idRestaurant: this.props.idRestaurant,
        content: this.state.comment,
      };
      const result = await this.props.onSubmit(this.props.token, data);
      if (result === true) {
        this.props.reloadComment(this.props.idRestaurant).then(() => {
          this.setState({ loading: false });
        });
      } else alert('Comment failed');
      this.setState({ comment: '' });
    }
  };

  reloadComment = () => {
    this.setState({ refreshing: true });
    this.props.reloadComment(this.props.idRestaurant);
    this.setState({ refreshing: false });
  };

  render() {
    return (
      <Comment
        data={this.props.data}
        onSubmit={this.onSubmit}
        loading={this.state.loading}
        comment={this.state.comment}
        refreshing={this.state.refreshing}
        reloadComment={this.reloadComment}
        onChangeComment={this.onChangeComment}
      />
    );
  }
}

const mapStateToProps = (state) => {
  console.log();
  return {
    token: state.AuthenReducer.token,
    accountReducer: state.AccountReducer,
  };
};

const mapDispatchToProps = (dispatch) => {
  console.log();
  return {
    onSubmit: async (token, data) => {
      const url = `${API}restaurant/addComment`;
      const response = await axios({
        url,
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data,
      });
      if (response.status === 200) {
        return true;
      }
      return false;
    },
    reloadComment: async (restaurantId) => {
      const url = `${API}restaurant/getAllCommentById/${restaurantId}`;
      try {
        const response = await axios({
          url,
          method: 'GET',
        });
        if (response.status === 200) {
          return dispatch(loadComment(response.data.result));
        }
      } catch (e) {
        console.log(e);
      }
      return null;
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CommentContainer);

import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { API } from '../../config/API';
import MenuList from '../../components/MerchantDetail/MenuList/MenuList';
import { loadOneMerchantData, loadImageAndVideo } from '../../actions';

class MenuListContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }

  componentDidMount() {
    this.props.getMenuList(this.props.id).then(() => {
      this.setState({ loading: false });
    });
  }

  render() {
    return <MenuList data={this.props.data} id={this.props.id} loading={this.state.loading} />;
  }
}

const mapStateToProps = state => ({
  data: state.MerchantDetailReducer.data,
  orderData: state.OrderReducer.data,
});

const mapDispatchToProps = dispatch => ({
  getMenuList: async (merchantId) => {
    const url = `${API}restaurant/getMenu/${merchantId}`;
    try {
      const response = await axios({
        url,
        method: 'GET',
      });
      if (response.status === 200) {
        dispatch(loadImageAndVideo(response.data.resource));
        return dispatch(loadOneMerchantData(response.data.menu));
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MenuListContainer);

import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { loadComment, viewComment } from '../../actions/index';
import { API } from '../../config/API';
import MerchantInfo from '../../components/MerchantDetail/MerchantInfo/MerchantInfo';

class MerchantInfoContainer extends React.Component {
  componentDidMount() {
    this.props.getComments(this.props.data.id);
  }

  render() {
    return (
      <MerchantInfo
        data={this.props.data}
        commentNumb={this.props.merchantDetailReducer.comments.length}
        viewComment={this.props.viewComment}
        isViewComment={this.props.merchantDetailReducer.viewComment}
        imageAndVideo={this.props.merchantDetailReducer.imageAndVideo}
      />
    );
  }
}

const mapStateToProps = (state) => {
  console.log();
  return {
    merchantDetailReducer: state.MerchantDetailReducer,
  };
};

const mapDispatchToProps = (dispatch) => {
  console.log();
  return {
    getComments: async (restaurantId) => {
      const url = `${API}restaurant/getAllCommentById/${restaurantId}`;
      try {
        const response = await axios({
          url,
          method: 'GET',
        });
        if (response.status === 200) {
          return dispatch(loadComment(response.data.result));
        }
      } catch (e) {
        console.log(e);
      }
      return null;
    },
    viewComment: () => dispatch(viewComment()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MerchantInfoContainer);

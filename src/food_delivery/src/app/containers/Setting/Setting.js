import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { API } from '../../config/API';
import Setting from '../../components/Setting/Setting';
import {
  getData,
  onEditing,
  updateAddress,
  updatePhone,
  updateUsername,
  updateAvatar,
  reset,
  updateRandomNumber,
  loadDistrictData,
  selectDistrict,
  getWardByDistrict,
  selectWard,
} from '../../actions/AccoutAction';

const ImagePicker = require('react-native-image-picker');

const options = {
  title: 'Import your image',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
  maxWidth: 750,
  maxHeight: 750,
};

class SettingContainer extends React.Component {
  edit = () => {
    const data = {
      userName: this.props.username === '' ? this.props.data.userName : this.props.username,
      phone: this.props.phone === '' ? this.props.data.phone : this.props.phone,
      idDistrict: this.props.districtAndWardReducer.currentDistrictId,
      idWard: this.props.districtAndWardReducer.currentWardId,
      street: this.props.address === '' ? this.props.data.address.street : this.props.address,
    };
    console.log(data);
    this.props.updateProfile(this.props.token, data);
    if (this.props.avatar !== null) {
      this.props.updateAvatar(this.props.avatar, this.props.token);
      this.props.updateRandomNumber();
    }
    this.props.getData(this.props.token);
  };

  openCamera = () => {};

  render() {
    return (
      <Setting
        avatar={this.props.avatar}
        randomNumber={this.props.randomNumber}
        data={this.props.data}
        editing={this.props.editing}
        onEditing={this.props.onEditing}
        updateUsername={this.props.updateUsername}
        updatePhone={this.props.updatePhone}
        updateAddress={this.props.updateAddress}
        edit={this.edit}
        changeAvatar={this.props.changeAvatar}
        resetEdited={this.props.reset}
        districtAndWardReducer={this.props.districtAndWardReducer}
        selectDistrict={this.props.selectDistrict}
        getWardByDistrict={this.props.getWardByDistrict}
        selectWard={this.props.selectWard}
      />
    );
  }
}

const mapStateToProps = state => ({
  districtAndWardReducer: state.AccountReducer,
  randomNumber: state.AccountReducer.randomNumber,
  avatar: state.AccountReducer.avatar,
  data: state.AccountReducer.data,
  editing: state.AccountReducer.editing,
  username: state.AccountReducer.username,
  phone: state.AccountReducer.phone,
  address: state.AccountReducer.address,
  token: state.AuthenReducer.token,
});

const mapDispatchToProps = dispatch => ({
  getData: async (token) => {
    const url = `${API}getinfo`;
    try {
      const response = await axios({
        url,
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      if (response.status === 200) {
        return dispatch(getData(response.data));
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  },
  getDistrictFromServer: async () => {
    const url = `${API}district/getAll`;
    try {
      const response = await axios({
        url,
        method: 'GET',
      });
      if (response.status === 200) {
        return dispatch(loadDistrictData(response.data));
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  },
  selectDistrict: data => dispatch(selectDistrict(data)),
  getWardByDistrict: async (districtId) => {
    const url = `${API}ward/getAllByDistrict?id=${districtId}`;
    try {
      const response = await axios({
        url,
        method: 'GET',
      });
      if (response.status === 200) {
        dispatch(selectWard(response.data[0].id));
        return dispatch(getWardByDistrict(response.data));
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  },
  selectWard: data => dispatch(selectWard(data)),
  onEditing: () => dispatch(onEditing()),
  updateUsername: data => dispatch(updateUsername(data)),
  updatePhone: data => dispatch(updatePhone(data)),
  updateAddress: data => dispatch(updateAddress(data)),
  updateProfile: async (token, data) => {
    const url = `${API}updateInfo`;
    try {
      const response = await axios({
        url,
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data,
      });
      if (response.status === 200) {
        console.log(response.data);
        alert('Update succesfull');
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  },
  changeAvatar: async () => {
    ImagePicker.showImagePicker(options, (response) => {
      // console.warn("URI: " + response.uri);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        return dispatch(updateAvatar(response.uri));
      }
      return null;
    });
  },
  updateAvatar: async (avatar, token) => {
    const data = new FormData();
    data.append('file', {
      uri: avatar,
      type: 'image/jpeg',
      name: 'avatar',
    });
    const url = `${API}updateAvatar`;
    try {
      const response = await axios({
        url,
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data,
      });
      if (response.status === 200) {
        console.log(response.data);
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  },
  reset: () => dispatch(reset()),
  updateRandomNumber: () => {
    const data = new Date().getTime();
    return dispatch(updateRandomNumber(data));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SettingContainer);

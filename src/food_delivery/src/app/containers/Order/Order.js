import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { API } from '../../config/API';
import { getOrderHistory, updateLoadingOrder } from '../../actions';
import Order from '../../components/Order/Order';

class OrderContainer extends React.Component {
  componentDidMount = async () => {
    const result = await this.props.getOrderHistoryData(this.props.token);
    const orderList = [];
    for (let i = 0; i < result.length; i++) {
      const item = result[i];
      if (item.idRestaurant !== null) {
        const restaurantName = await this.props.getRestaurantData(item.idRestaurant);
        const data = {
          ...item,
          restaurantName,
        };
        orderList.push(data);
      }
    }
    this.props.getOrderHistory(orderList);
    this.props.updateLoadingOrder(false);
  };

  render() {
    return <Order data={this.props.orderReducer.orderHistoryData} />;
  }
}

const mapStateToProps = state => ({
  token: state.AuthenReducer.token,
  orderReducer: state.OrderReducer,
});

const mapDispatchToProps = dispatch => ({
  getOrderHistoryData: async (token) => {
    const url = `${API}order/getAll`;
    try {
      const response = await axios({
        url,
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      if (response.status === 200) {
        return response.data;
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  },
  getRestaurantData: async (restaurantId) => {
    const url = `${API}restaurant/getMenu/${restaurantId}`;
    try {
      const response = await axios({
        url,
        method: 'GET',
      });
      if (response.status === 200) {
        return response.data.restaurant.name;
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  },
  getOrderHistory: (data) => {
    dispatch(getOrderHistory(data));
  },
  updateLoadingOrder: data => dispatch(updateLoadingOrder(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(OrderContainer);

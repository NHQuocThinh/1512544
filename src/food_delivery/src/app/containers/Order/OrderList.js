import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import { API } from '../../config/API';
import { getOrderHistory } from '../../actions';
import OrderList from '../../components/Order/OrderList/OrderList';

class OrderListContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  navigateToOrderDetail = (input) => {
    this.props.navigation.navigate('OrderDetail', { data: input });
  };

  onRefresh = async () => {
    this.setState({ refreshing: true });
    const result = await this.props.getOrderHistoryData(this.props.token);
    const orderList = [];
    for (let i = 0; i < result.length; i++) {
      const item = result[i];
      if (item.idRestaurant !== null) {
        const restaurantName = await this.props.getRestaurantData(item.idRestaurant);
        const data = {
          ...item,
          restaurantName,
        };
        orderList.push(data);
      }
    }
    this.props.getOrderHistory(orderList);
    this.setState({ refreshing: false });
  };

  render() {
    return (
      <OrderList
        data={this.props.data}
        navigateToOrderDetail={this.navigateToOrderDetail}
        loading={this.props.orderReducer.loading}
        refreshing={this.state.refreshing}
        onRefresh={this.onRefresh}
      />
    );
  }
}

const mapStateToProps = (state) => {
  console.log();
  return {
    token: state.AuthenReducer.token,
    orderReducer: state.OrderReducer,
  };
};

const mapDispatchToProps = (dispatch) => {
  console.log();
  return {
    getOrderHistoryData: async (token) => {
      const url = `${API}order/getAll`;
      try {
        const response = await axios({
          url,
          method: 'GET',
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        if (response.status === 200) {
          return response.data;
        }
      } catch (e) {
        console.log(e);
      }
      return null;
    },
    getRestaurantData: async (restaurantId) => {
      const url = `${API}restaurant/getMenu/${restaurantId}`;
      try {
        const response = await axios({
          url,
          method: 'GET',
        });
        if (response.status === 200) {
          return response.data.restaurant.name;
        }
      } catch (e) {
        console.log(e);
      }
      return null;
    },
    getOrderHistory: (data) => {
      dispatch(getOrderHistory(data));
    },
  };
};

export default withNavigation(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(OrderListContainer),
);

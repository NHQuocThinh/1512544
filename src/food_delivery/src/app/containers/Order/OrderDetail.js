import React from 'react';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import axios from 'axios';
import { API } from '../../config/API';
import OrderDetail from '../../components/Order/OrderDetail/OrderDetail';

class OrderDetailContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentDidMount = async () => {
    const { id, idRestaurant } = this.props.navigation.state.params.data;
    const orderFoods = await this.props.getOrderDetail(id, this.props.token);
    const restaurantMenu = await this.props.getRestaurantMent(idRestaurant);
    const foodsData = [];
    for (let i = 0; i < orderFoods.length; i++) {
      const foodData = restaurantMenu.filter(food => orderFoods[i].idFood === food.id);

      const tmp = {
        data: foodData,
        quantity: orderFoods[i].quantity,
      };
      foodsData.push(tmp);
    }
    const orderDetail = {
      ...this.props.navigation.state.params.data,
      details: foodsData,
    };
    this.setState({ data: orderDetail });
  };

  render() {
    return <OrderDetail data={this.state.data} />;
  }
}

const mapStateToProps = (state) => {
  console.log();
  return {
    token: state.AuthenReducer.token,
  };
};

const mapDispatchToProps = () => {
  console.log();
  return {
    getOrderDetail: async (orderId, token) => {
      const url = `${API}order/getOrder/${orderId}`;
      try {
        const response = await axios({
          url,
          method: 'GET',
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        if (response.status === 200) {
          return response.data.details;
        }
      } catch (e) {
        console.log(e);
      }
      return null;
    },
    getRestaurantMent: async (restaurantId) => {
      const url = `${API}restaurant/getMenu/${restaurantId}`;
      try {
        const response = await axios({
          url,
          method: 'GET',
        });
        if (response.status === 200) {
          return response.data.menu;
        }
      } catch (e) {
        console.log(e);
      }
      return null;
    },
  };
};

export default withNavigation(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(OrderDetailContainer),
);

import React from 'react';
import {
  View, StyleSheet, Text, ActivityIndicator,
} from 'react-native';
import axios from 'axios';
import { connect } from 'react-redux';
import CategoryList from '../components/CategoryList/CategoryList';
import { API } from '../config/API';
import { loadCategoryData } from '../actions';
import I18n from '../config/I18n';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
    alignItems: 'center',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    backgroundColor: '#283593',
    paddingHorizontal: 10,
  },
  headerTitle: {
    marginLeft: 20,
    fontSize: 20,
    fontWeight: 'bold',
    letterSpacing: 1.1,
    color: '#fff',
  },
  imageContainer: { width: 20, height: 20 },
  indicatorContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
});

class Category extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
    this.props.getCategoryDataFromServer();
    setTimeout(() => {
      this.setState({ loading: false });
    }, 1500);
  }

  static navigationOptions = () => ({
    header: (
      <View style={styles.headerContainer}>
        <Text style={styles.headerTitle}>{I18n.t('Category_Header')}</Text>
      </View>
    ),
  });

  render() {
    if (this.state.loading) {
      return (
        <View style={styles.indicatorContainer}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <CategoryList data={this.props.data} />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  data: state.CategoryReducer.data,
});

const mapDispatchToProps = dispatch => ({
  getCategoryDataFromServer: async () => {
    const url = `${API}categories/getAll`;
    try {
      const response = await axios({
        url,
        method: 'GET',
      });
      if (response.status === 200) {
        return dispatch(loadCategoryData(response.data));
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Category);

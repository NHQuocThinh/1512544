import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { API } from '../../config/API';
import { getNotification } from '../../actions/index';
import Notification from '../../components/Notification/Notification';

class NotificationContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true,
    };
  }

  componentDidMount() {
    this.props.getNotification(this.props.token).then(() => {
      this.setState({ loading: false });
    });
  }

  onRefresh = () => {
    this.setState({ refreshing: true });
    this.props.getNotification(this.props.token);
    this.setState({ refreshing: false });
  };

  render() {
    return (
      <Notification
        data={this.props.notificationReducer.data}
        refreshing={this.state.refreshing}
        onRefresh={this.onRefresh}
        loading={this.state.loading}
      />
    );
  }
}

const mapStateToProps = state => ({
  token: state.AuthenReducer.token,
  notificationReducer: state.NotificationReducer,
});

const mapDispatchToProps = dispatch => ({
  getNotification: async (token) => {
    const url = `${API}getNoti`;
    const response = await axios({
      url,
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    if (response.status === 200) {
      return dispatch(getNotification(response.data));
    }
    return null;
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NotificationContainer);

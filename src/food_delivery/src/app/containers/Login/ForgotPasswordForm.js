import React from 'react';
import { withNavigation } from 'react-navigation';
import axios from 'axios';
import { API } from '../../config/API';
import ForgotPasswordForm from '../../components/Login/ForgotPassword/ForgotPasswordForm/ForgotPasswordForm';

class ForgotPasswordFormContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      loading: false,
      errorMsg: 'nothing',
    };
  }

  onPressSendMyPassword = async () => {
    this.setState({ loading: true });
    const url = `${API}forgetPassword`;
    const data = {
      email: this.state.email,
    };
    let response;
    try {
      response = await axios({
        url,
        method: 'POST',
        data,
      });
      if (response.status === 200) {
        this.setState({ errorMsg: 'nothing' });
        this.props.navigation.goBack();
      }
    } catch (e) {
      this.setState({
        errorMsg: e.response.data.msg,
      });
    }
    this.setState({ loading: false });
  };

  onChangeEmail = (data) => {
    this.setState({ email: data });
  };

  navigateBackToLogin = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <ForgotPasswordForm
        email={this.state.email}
        loading={this.state.loading}
        errorMsg={this.state.errorMsg}
        onPressSendMyPassword={this.onPressSendMyPassword}
        onChangeEmail={this.onChangeEmail}
        navigate={this.navigateBackToLogin}
      />
    );
  }
}

export default withNavigation(ForgotPasswordFormContainer);

import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import { API } from '../../config/API';
import { authenSuccess } from '../../actions';
import LoginForm from '../../components/Login/LoginForm/LoginForm';
import {
  getData,
  loadDistrictData,
  selectDistrict,
  selectWard,
  getWardByDistrict,
} from '../../actions/AccoutAction';

class LoginFormContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errorMsg: 'nothing',
      loading: false,
    };
  }

  componentDidMount = async () => {
    const { isAuthenticated, userInfo, token } = this.props.authen;
    if (isAuthenticated === true) {
      this.setState({
        email: userInfo.email,
        password: userInfo.password,
      });
      this.props.getData(token).then(() => {
        if (this.props.account.currentDistrictId !== '') {
          console.log(this.props.account.data.address.street);
          this.props.getDistrictFromServer();
          this.props.getWardByDistrict(this.props.account.currentDistrictId);
        }
      });
    } else {
      this.setState({ loading: false });
    }
    this.setState({
      email: userInfo.email,
      password: userInfo.password,
    });
  };

  onPressLogin = async () => {
    this.setState({ loading: true });
    const url = `${API}login`;
    const data = {
      email: this.state.email,
      password: this.state.password,
    };
    let response;
    try {
      response = await axios({
        url,
        method: 'POST',
        data,
      });
      if (response.status === 200) {
        const authenData = {
          userInfo: {
            email: data.email,
            password: data.password,
          },
          token: response.data.token,
        };
        this.props.authenSuccess(authenData);
        this.props.navigation.navigate('BottomTabNavigator');
      }
    } catch (e) {
      this.setState({ errorMsg: 'Wrong email or password' });
    }
    this.setState({ loading: false });
  };

  onChangeEmail = (data) => {
    this.setState({ email: data });
  };

  onChangePassword = (data) => {
    this.setState({ password: data });
  };

  render() {
    return (
      <LoginForm
        email={this.state.email}
        password={this.state.password}
        errorMsg={this.state.errorMsg}
        loading={this.state.loading}
        onChangeEmail={this.onChangeEmail}
        onChangePassword={this.onChangePassword}
        onPressLogin={this.onPressLogin}
      />
    );
  }
}

const mapStateToProps = state => ({
  authen: state.AuthenReducer,
  account: state.AccountReducer,
});

const mapDispatchToProps = dispatch => ({
  authenSuccess: data => dispatch(authenSuccess(data)),
  getData: async (token) => {
    const url = `${API}getinfo`;
    try {
      const response = await axios({
        url,
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      if (response.status === 200) {
        dispatch(getData(response.data));
        if (response.data.address.idDistrict !== null) {
          dispatch(selectDistrict(response.data.address.idDistrict));
        }
        if (response.data.address.idWard !== null) {
          dispatch(selectWard(response.data.address.idWard));
        }
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  },
  getDistrictFromServer: async () => {
    const url = `${API}district/getAll`;
    try {
      const response = await axios({
        url,
        method: 'GET',
      });
      if (response.status === 200) {
        return dispatch(loadDistrictData(response.data));
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  },
  getWardByDistrict: async (districtId) => {
    const url = `${API}ward/getAllByDistrict?id=${districtId}`;
    try {
      const response = await axios({
        url,
        method: 'GET',
      });
      if (response.status === 200) {
        return dispatch(getWardByDistrict(response.data));
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  },
});

export default withNavigation(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(LoginFormContainer),
);

import React from 'react';
import { connect } from 'react-redux';
import { authenSuccess } from '../../actions';
import Login from '../../components/Login/Login';

class LoginContainer extends React.Component {
  navigate = (screen) => {
    this.props.navigation.navigate(screen);
  };

  render() {
    return (
      <Login
        authenSuccess={this.props.authenSuccess}
        authen={this.props.authen}
        navigate={this.navigate}
      />
    );
  }
}

const mapStateToProps = state => ({
  authen: state.AuthenReducer,
});

const mapDispatchToProps = dispatch => ({
  authenSuccess: data => dispatch(authenSuccess(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginContainer);

import React from 'react';
import ForgotPassword from '../../components/Login/ForgotPassword/ForgotPassword';

class ForgotPasswordContainer extends React.Component {
  render() {
    return <ForgotPassword />;
  }
}

export default ForgotPasswordContainer;

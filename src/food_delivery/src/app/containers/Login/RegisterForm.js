import React, { Component } from 'react';
import { withNavigation } from 'react-navigation';
import axios from 'axios';
import { API } from '../../config/API';
import RegisterForm from '../../components/Login/Register/RegisterForm/RegisterForm';

class RegisterFormContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      rePassword: '',
      errorMsg: 'nothing',
      loading: false,
    };
  }

  onPressRegister = async () => {
    this.setState({ loading: true });
    const url = `${API}register`;
    const data = {
      email: this.state.email,
      password: this.state.password,
    };
    let response;
    try {
      response = await axios({
        url,
        method: 'POST',
        data,
      });
      if (response.status === 200) {
        const authenData = {
          userInfo: {
            email: data.email,
            password: data.password,
          },
          token: '',
        };
        this.props.authenSuccess(authenData);
        this.props.navigation.goBack();
      }
    } catch (e) {
      this.setState({ errorMsg: e.response.data.msg, error: true });
    }
    this.setState({ loading: false });
  };

  onChangeEmail = (data) => {
    this.setState({ email: data });
  };

  onChangePassword = (data) => {
    this.setState({ password: data });
  };

  onChangeRePassword = (data) => {
    this.setState({ rePassword: data });
  };

  navigateBackToLogin = () => {
    this.props.navigation.goBack();
  }

  render() {
    return (
      <RegisterForm
        email={this.state.email}
        password={this.state.password}
        rePassword={this.state.rePassword}
        errorMsg={this.state.errorMsg}
        loading={this.state.loading}
        onChangeEmail={this.onChangeEmail}
        onChangePassword={this.onChangePassword}
        onChangeRePassword={this.onChangeRePassword}
        onPressRegister={this.onPressRegister}
        navigateBackToLogin={this.navigateBackToLogin}
      />
    );
  }
}

export default withNavigation(RegisterFormContainer);

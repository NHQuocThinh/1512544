import React, { Component } from 'react';
import { connect } from 'react-redux';
import { authenSuccess } from '../../actions';
import Register from '../../components/Login/Register/Register';

class RegisterContainer extends Component {
  render() {
    return <Register authenSuccess={this.props.authenSuccess} authen={this.props.authen}/>;
  }
}

const mapStateToProps = state => ({
  authen: state.AuthenReducer,
});

const mapDispatchToProps = dispatch => ({
  authenSuccess: data => dispatch(authenSuccess(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RegisterContainer);

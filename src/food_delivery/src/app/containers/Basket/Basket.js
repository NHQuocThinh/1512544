import React from 'react';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';
import axios from 'axios';
import { API } from '../../config/API';
import Basket from '../../components/Basket/Basket';

const calculateTotalPrice = (data) => {
  const totalPrice = data.reduce((price, item) => (price += item.price * item.quantity), 0);
  return totalPrice;
};

class BasketContainer extends React.Component {
  submitOrder = async () => {
    const { token } = this.props.authenReducer;
    const totalPrice = calculateTotalPrice(this.props.orderReducer.data);
    const result = await this.props.submitOrder(this.props.orderReducer, token, totalPrice);
    if (result === true) {
      alert('Submit order success. Please check it in order tab');
    } else {
      lert('Submit order fail');
    }
    this.props.navigation.goBack();
  };

  render() {
    return <Basket orderData={this.props.orderReducer.data} submitOrder={this.submitOrder} />;
  }
}

const mapStateToProps = (state) => {
  console.log();
  return {
    orderReducer: state.OrderReducer,
    authenReducer: state.AuthenReducer,
  };
};

const mapDispatchToProps = () => ({
  submitOrder: async (orderData, token, totalPrice) => {
    const items = orderData.data.map(item => ({
      idFood: item.idFood,
      quantity: item.quantity,
    }));
    const data = {
      totalPrice,
      Street: orderData.street,
      idWard: orderData.currentWardId,
      idDistrict: orderData.currentDistrictId,
      phone: orderData.phone,
      idRestaurant: orderData.restaurantId,
      item: items,
    };
    const url = `${API}order/create`;
    try {
      const response = await axios({
        url,
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data,
      });
      if (response.status === 200) {
        return true;
      }
    } catch (e) {
      console.log(e);
    }
    return false;
  },
});

export default withNavigation(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(BasketContainer),
);

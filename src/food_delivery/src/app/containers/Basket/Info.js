import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import Info from '../../components/Basket/Info/Info';
import {
  updateUsernameOrder,
  updatePhoneOrder,
  updateStreetOrder,
  updateDistrictDataOrder,
  updateWardDataOrder,
  updateCurrentDistrictIdOrder,
  updateCurrentWardIdOrder,
} from '../../actions/index';
import { API } from '../../config/API';

class InfoContainer extends React.Component {
  componentDidMount() {
    this.props.getDataFromUserProfile(this.props.accountReducer);
  }

  render() {
    return (
      <Info
        accountReducer={this.props.accountReducer}
        orderReducer={this.props.orderReducer}
        updateCurrentDistrictIdOrder={this.props.updateCurrentDistrictIdOrder}
        updateCurrentWardIdOrder={this.props.updateCurrentWardIdOrder}
        getWardFromDistrictId={this.props.getWardFromDistrictId}
        updateUsernameOrder={this.props.updateUsernameOrder}
        updatePhoneOrder={this.props.updatePhoneOrder}
        updateStreetOrder={this.props.updateStreetOrder}
      />
    );
  }
}

const mapStateToProps = (state) => {
  console.log();
  return {
    accountReducer: state.AccountReducer,
    orderReducer: state.OrderReducer,
  };
};

const mapDispatchToProps = (dispatch) => {
  console.log();
  return {
    getDataFromUserProfile: (userProfile) => {
      dispatch(updateUsernameOrder(userProfile.data.userName));
      dispatch(updatePhoneOrder(userProfile.data.phone));
      dispatch(updateStreetOrder(userProfile.data.address.street));
      dispatch(updateDistrictDataOrder(userProfile.districtData));
      dispatch(updateWardDataOrder(userProfile.wardData));
      dispatch(updateCurrentDistrictIdOrder(userProfile.currentDistrictId));
      dispatch(updateCurrentWardIdOrder(userProfile.currentWardId));
    },
    updateCurrentDistrictIdOrder: data => dispatch(updateCurrentDistrictIdOrder(data)),
    updateCurrentWardIdOrder: data => dispatch(updateCurrentWardIdOrder(data)),
    updateUsernameOrder: data => dispatch(updateUsernameOrder(data)),
    updatePhoneOrder: data => dispatch(updatePhoneOrder(data)),
    updateStreetOrder: data => dispatch(updateStreetOrder(data)),
    getWardFromDistrictId: async (districtId) => {
      const url = `${API}ward/getAllByDistrict?id=${districtId}`;
      try {
        const response = await axios({
          url,
          method: 'GET',
        });
        if (response.status === 200) {
          dispatch(updateCurrentWardIdOrder(response.data[0].id));
          return dispatch(updateWardDataOrder(response.data));
        }
      } catch (e) {
        console.log(e);
      }
      return null;
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InfoContainer);

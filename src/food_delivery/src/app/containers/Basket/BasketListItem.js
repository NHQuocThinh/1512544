import React from 'react';
import { connect } from 'react-redux';
import BasketListItem from '../../components/Basket/BasketItem/BasketItem';
import { addNewFood, removeFood } from '../../actions/index';

class BasketListItemContainer extends React.Component {
  addNewFoodToYourOrder = (data) => {
    let added = false;
    const oldOrderData = this.props.OrderReducer.data;
    const newItem = {
      idFood: data.idFood,
      price: data.price,
      name: data.name,
      note: '',
      quantity: 1,
    };
    const newOrderData = oldOrderData.map((item) => {
      if (item.idFood === newItem.idFood) {
        added = true;
        return { ...item, quantity: item.quantity + 1 };
      }
      return item;
    });
    if (added === false) {
      newOrderData.push(newItem);
    }
    this.props.addNewFood(newOrderData);
  };

  removeFoodFromYourOrder = (data) => {
    let removedIndex = -1;
    const oldOrderData = this.props.OrderReducer.data;
    const removedItem = {
      idFood: data.idFood,
    };
    const newOrderData = oldOrderData.map((item, index) => {
      if (item.idFood === removedItem.idFood) {
        removed = true;
        if (item.quantity - 1 === 0) removedIndex = index;
        return { ...item, quantity: item.quantity - 1 };
      }
      return item;
    });
    if (removedIndex !== -1) {
      newOrderData.splice(removedIndex, 1);
    }
    this.props.removeFood(newOrderData);
  };

  render() {
    return (
      <BasketListItem
        data={this.props.data}
        addNewFoodToYourOrder={this.addNewFoodToYourOrder}
        removeFoodFromYourOrder={this.removeFoodFromYourOrder}
      />
    );
  }
}

const mapStateToProps = (state) => {
  console.log();
  return {
    OrderReducer: state.OrderReducer,
  };
};

const mapDispatchToProps = (dispatch) => {
  console.log();
  return {
    addNewFood: data => dispatch(addNewFood(data)),
    removeFood: data => dispatch(removeFood(data)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BasketListItemContainer);

import React from 'react';
import { connect } from 'react-redux';
import BasketItem from '../../components/Basket/BasketList/BasketList';

class BasketListContainer extends React.Component {
  render() {
    return <BasketItem data={this.props.OrderReducer.data} />;
  }
}

const mapStateToProps = (state) => {
  console.log();
  return {
    OrderReducer: state.OrderReducer,
  };
};

const mapDispatchToProps = () => {
  console.log();
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BasketListContainer);

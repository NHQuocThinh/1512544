import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { Dimensions } from 'react-native';
import { debounce } from 'throttle-debounce';
import { withNavigation } from 'react-navigation';
import { ProviderPropType } from 'react-native-maps';
import { API } from '../../config/API';
import Map from '../../components/Map/Map';
import { loadMerchantsLocation } from '../../actions';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 10.762797;
const LONGITUDE = 106.681193;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class MapContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      markers: [],
      currentLocation: null,
    };
    this.changeRegionDebounce = debounce(500, this.changeRegion);
  }

  changeRegion = (lattitude, longitude) => {
    this.props.loadMerchantsLocation(lattitude, longitude);
  };

  componentDidMount = () => {
    navigator.geolocation.getCurrentPosition((position) => {
      console.log(position);
      const coordinate = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
      };
      this.setState({ currentLocation: coordinate });
      this.props.loadMerchantsLocation(position.coords.latitude, position.coords.longitude);
    });
  };

  navigateBackToListView = (data) => {
    this.props.navigation.navigate('MerchantDetail_MerchantStack', { data });
  };

  render() {
    const markers = this.props.data.map(item => ({
      ...item,
      coordinate: {
        latitude: item.latitude,
        longitude: item.longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
    }));
    return (
      <Map
        provide={this.props.provider}
        currentLocation={this.state.currentLocation}
        markers={markers}
        changeRegionDebounce={this.changeRegionDebounce}
        region={this.state.region}
        navigateBackToListView={this.navigateBackToListView}
      />
    );
  }
}

MapContainer.propTypes = {
  provider: ProviderPropType,
};

const mapStateToProps = state => ({
  data: state.MerchantsLocation.data,
});

const mapDispatchToProps = dispatch => ({
  loadMerchantsLocation: async (lat, long) => {
    const url = `${API}restaurant/nearMe/${lat}&${long}`;
    try {
      const response = await axios({
        url,
        method: 'GET',
      });
      if (response.status === 200) {
        return dispatch(loadMerchantsLocation(response.data));
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  },
});

export default withNavigation(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(MapContainer),
);

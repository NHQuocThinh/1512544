const intialState = {
  isAuthenticated: false,
  userInfo: null,
  token: null,
};

const AuthenReducer = (state = intialState, action) => {
  switch (action.type) {
    case 'AuthenReducer_SUCCESS': {
      return {
        ...state,
        isAuthenticated: true,
        userInfo: action.userInfo,
        token: action.token,
      };
    }
    default:
      return state;
  }
};

export default AuthenReducer;

const initialState = {
  data: [],
  imageAndVideo: [],
  comments: [],
  viewComment: false,
};

const MerchantDetailReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'MerchantDetailReducer_LOAD_DATA': {
      return {
        ...state,
        data: action.data,
      };
    }
    case 'MerchantDetailReducer_LOAD_COMMENT': {
      return {
        ...state,
        comments: action.data,
      };
    }
    case 'MerchantDetailReducer_VIEW_COMMENT': {
      return {
        ...state,
        viewComment: !state.viewComment,
      };
    }
    case 'MerchantDetailReducer_LOAD_IMAGE_AND_VIDEO': {
      return {
        ...state,
        imageAndVideo: action.data,
      };
    }
    default:
      return state;
  }
};

export default MerchantDetailReducer;

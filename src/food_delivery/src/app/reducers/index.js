import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import RestaurantReducer from './RestaurantReducer';
import MerchantDetailReducer from './MerchantDetailReducer';
import OrderReducer from './OrderReducer';
import DistrictAndWardReducer from './DistrictAndWardReducer';
import AuthenReducer from './AuthenReducer';
import MerchantsLocation from './MerchantsLocationReducer';
import AccountReducer from './AccountReducer';
import NotificationReducer from './NotificationReducer';

const authenticationConfig = {
  key: 'authentication',
  storage,
  blacklist: ['isAuthenticating'],
};

export default combineReducers({
  AuthenReducer: persistReducer(authenticationConfig, AuthenReducer),
  RestaurantReducer,
  MerchantDetailReducer,
  OrderReducer,
  DistrictAndWardReducer,
  MerchantsLocation,
  AccountReducer,
  NotificationReducer,
});

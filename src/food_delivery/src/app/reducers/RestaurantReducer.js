const initialState = {
  data: [],
  currentPage: 1,
  isSearching: false,
  loading: false,
};

const RestaurantReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'RestaurantReducer_LOAD_DATA': {
      return {
        ...state,
        data: action.data,
        isSearching: action.isSearching,
      };
    }
    case 'RestaurantReducer_SEARCH_DATA': {
      return {
        ...state,
        data: action.data,
        isSearching: action.isSearching,
      };
    }
    case 'RestaurantReducer_UPDATE_CURRENTPAGE': {
      return {
        ...state,
        currentPage: action.data,
      };
    }
    default:
      return state;
  }
};

export default RestaurantReducer;

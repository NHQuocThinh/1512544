import {
  GET_DATA,
  EDITING,
  UPDATE_ADDRESS,
  UPDATE_PHONE,
  UPDATE_USERNAME,
  UPDATE_AVATAR,
  RESET,
  UPDATE_RANDOMNUMBER,
} from '../actions/AccoutAction';

const intialState = {
  data: {},
  editing: false,
  username: '',
  phone: '',
  address: '',
  avatar: null,
  randomNumber: new Date().getTime(),
  districtData: [],
  wardData: [],
  currentDistrictId: '',
  currentWardId: '',
};

const AccountReducer = (state = intialState, action) => {
  switch (action.type) {
    case GET_DATA: {
      return {
        ...state,
        data: action.data,
      };
    }
    case EDITING: {
      return {
        ...state,
        editing: !state.editing,
      };
    }
    case UPDATE_ADDRESS: {
      return {
        ...state,
        address: action.data,
      };
    }
    case UPDATE_USERNAME: {
      return {
        ...state,
        username: action.data,
      };
    }
    case UPDATE_PHONE: {
      return {
        ...state,
        phone: action.data,
      };
    }
    case UPDATE_AVATAR: {
      return {
        ...state,
        avatar: action.data,
      };
    }
    case UPDATE_RANDOMNUMBER: {
      return {
        ...state,
        randomNumber: action.data,
      };
    }
    case 'DistrictAndWardReducer_LOAD_DISTRICT_DATA': {
      return {
        ...state,
        districtData: action.data,
      };
    }
    case 'DistrictAndWardReducer_SELECT_DISTRICT': {
      return {
        ...state,
        currentDistrictId: action.data,
      };
    }
    case 'DistrictAndWardReducer_LOAD_WARD_BY_DISTRICT': {
      return {
        ...state,
        wardData: action.data,
      };
    }
    case 'DistrictAndWardReducer_SELECT_WARD': {
      return {
        ...state,
        currentWardId: action.data,
      };
    }
    case RESET: {
      return {
        ...state,
        editing: false,
        username: '',
        phone: '',
        address: '',
        avatar: null,
      };
    }
    default:
      return state;
  }
};

export default AccountReducer;

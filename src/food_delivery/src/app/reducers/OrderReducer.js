const initialState = {
  data: [],
  orderHistoryData: [],
  username: '',
  phone: '',
  street: '',
  districtData: [],
  wardData: [],
  currentDistrictId: '',
  currentWardId: '',
  restaurantId: '',
  loading: true,
};

const OrderReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'OrderReducer_ADD_FOOD': {
      return {
        ...state,
        data: action.data,
      };
    }
    case 'OrderReducer_REMOVE_FOOD': {
      return {
        ...state,
        data: action.data,
      };
    }
    case 'OrderReducer_RESET_ORDER': {
      return {
        ...state,
        data: action.data,
      };
    }
    case 'OrderReducer_GET_HISTORY_ORDER': {
      return {
        ...state,
        orderHistoryData: action.data,
      };
    }
    case 'OrderReducer_UPDATE_USERNAME': {
      return {
        ...state,
        username: action.data,
      };
    }
    case 'OrderReducer_UPDATE_PHONE': {
      return {
        ...state,
        phone: action.data,
      };
    }
    case 'OrderReducer_UPDATE_STREET': {
      return {
        ...state,
        street: action.data,
      };
    }
    case 'OrderReducer_UPDATE_DISTRICT_DATA': {
      return {
        ...state,
        districtData: action.data,
      };
    }
    case 'OrderReducer_UPDATE_WARD_DATA': {
      return {
        ...state,
        wardData: action.data,
      };
    }
    case 'OrderReducer_UPDATE_CURRENT_DISTRICT_ID': {
      return {
        ...state,
        currentDistrictId: action.data,
      };
    }
    case 'OrderReducer_UPDATE_CURRENT_WARD_ID': {
      return {
        ...state,
        currentWardId: action.data,
      };
    }
    case 'OrderReducer_UPDATE_RESTAURANT_ID': {
      return {
        ...state,
        restaurantId: action.data,
      };
    }
    case 'OrderReducer_UPDATE_LOADING': {
      return {
        ...state,
        loading: action.data,
      };
    }
    default:
      return state;
  }
};

export default OrderReducer;

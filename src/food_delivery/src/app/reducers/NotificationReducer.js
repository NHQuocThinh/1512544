const initialState = {
  data: [],
};

const NotificationReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'NotificationReducer_LOAD_DATA': {
      return {
        ...state,
        data: action.data,
      };
    }
    default:
      return state;
  }
};

export default NotificationReducer;

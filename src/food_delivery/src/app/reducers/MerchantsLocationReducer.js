const initialState = {
  data: [],
};

const DistrictAndWardReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'MerchantsLocationReducer_LOAD_DATA': {
      return {
        ...state,
        data: action.data,
      };
    }
    default:
      return state;
  }
};

export default DistrictAndWardReducer;

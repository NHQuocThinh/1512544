const initialState = {
  districtData: [],
  wardData: [],
  currentDistrictId: '',
  currentWardId: '',
  phoneNumber: '',
  street: '',
};

const DistrictAndWardReducer = (state = initialState, action) => {
  switch (action.type) {
    // case 'DistrictAndWardReducer_LOAD_DISTRICT_DATA': {
    //   return {
    //     ...state,
    //     districtData: action.data,
    //   };
    // }
    // case 'DistrictAndWardReducer_SELECT_DISTRICT': {
    //   return {
    //     ...state,
    //     currentDistrictId: action.data,
    //   };
    // }
    // case 'DistrictAndWardReducer_LOAD_WARD_BY_DISTRICT': {
    //   return {
    //     ...state,
    //     wardData: action.data,
    //   };
    // }
    // case 'DistrictAndWardReducer_SELECT_WARD': {
    //   return {
    //     ...state,
    //     currentWardId: action.data,
    //   };
    // }
    // case 'DistrictAndWardReducer_RESET_DATA': {
    //   return {
    //     ...state,
    //     districtData: [],
    //     wardData: [],
    //     currentDistrictId: '',
    //     currentWardId: '',
    //     phoneNumber: '',
    //     street: '',
    //   };
    // }
    default:
      return state;
  }
};

export default DistrictAndWardReducer;

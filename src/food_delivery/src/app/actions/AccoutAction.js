export const GET_DATA = 'GET_DATA';
export const EDITING = 'EDITING';
export const UPDATE_USERNAME = 'UPDATE_USERNAME';
export const UPDATE_PHONE = 'UPDATE_PHONE';
export const UPDATE_ADDRESS = 'UPDATE_ADDRESS';
export const UPDATE_AVATAR = 'UPDATE_AVATAR';
export const RESET = 'RESET';
export const UPDATE_RANDOMNUMBER = 'UPDATE_RANDOMNUMBER';

export const getData = input => ({
  type: GET_DATA,
  data: input,
});

export const onEditing = () => ({
  type: EDITING,
});

export const updateUsername = input => ({
  type: UPDATE_USERNAME,
  data: input,
});

export const updatePhone = input => ({
  type: UPDATE_PHONE,
  data: input,
});

export const updateAddress = input => ({
  type: UPDATE_ADDRESS,
  data: input,
});

export const updateAvatar = input => ({
  type: UPDATE_AVATAR,
  data: input,
});

export const updateRandomNumber = input => ({
  type: UPDATE_RANDOMNUMBER,
  data: input,
});

export const reset = () => ({
  type: RESET,
});

export const loadDistrictData = input => ({
  type: 'DistrictAndWardReducer_LOAD_DISTRICT_DATA',
  data: input,
});

export const selectDistrict = input => ({
  type: 'DistrictAndWardReducer_SELECT_DISTRICT',
  data: input,
});

export const getWardByDistrict = input => ({
  type: 'DistrictAndWardReducer_LOAD_WARD_BY_DISTRICT',
  data: input,
});

export const selectWard = input => ({
  type: 'DistrictAndWardReducer_SELECT_WARD',
  data: input,
});

/*
    Authen Reducer actions
*/

export const authenSuccess = input => ({
  type: 'AuthenReducer_SUCCESS',
  userInfo: input.userInfo,
  token: input.token,
});

/*
    Restaurant Reducer actions
*/
export const loadRestaurantData = input => ({
  type: 'RestaurantReducer_LOAD_DATA',
  data: input,
  isSearching: false,
});

export const searchingRestaurant = input => ({
  type: 'RestaurantReducer_SEARCH_DATA',
  data: input,
  isSearching: true,
});

export const updateCurrentPage = input => ({
  type: 'RestaurantReducer_UPDATE_CURRENTPAGE',
  data: input,
});

/*
    Category Reducer actions
*/

export const loadCategoryData = input => ({
  type: 'CategoryReducer_LOAD_DATA',
  data: input,
});

/*
    Merchant detail Reducer actions
*/

export const loadOneMerchantData = input => ({
  type: 'MerchantDetailReducer_LOAD_DATA',
  data: input,
});

export const loadComment = input => ({
  type: 'MerchantDetailReducer_LOAD_COMMENT',
  data: input,
});

export const viewComment = () => ({
  type: 'MerchantDetailReducer_VIEW_COMMENT',
});

export const loadImageAndVideo = input => ({
  type: 'MerchantDetailReducer_LOAD_IMAGE_AND_VIDEO',
  data: input,
});
/*
    Order Reducer actions
*/

export const addNewFood = input => ({
  type: 'OrderReducer_ADD_FOOD',
  data: input,
});

export const removeFood = input => ({
  type: 'OrderReducer_REMOVE_FOOD',
  data: input,
});

export const resetOrder = () => ({
  type: 'OrderReducer_RESET_ORDER',
  data: [],
});

export const getOrderHistory = input => ({
  type: 'OrderReducer_GET_HISTORY_ORDER',
  data: input,
});

export const updateUsernameOrder = input => ({
  type: 'OrderReducer_UPDATE_USERNAME',
  data: input,
});

export const updatePhoneOrder = input => ({
  type: 'OrderReducer_UPDATE_PHONE',
  data: input,
});

export const updateStreetOrder = input => ({
  type: 'OrderReducer_UPDATE_STREET',
  data: input,
});

export const updateDistrictDataOrder = input => ({
  type: 'OrderReducer_UPDATE_DISTRICT_DATA',
  data: input,
});

export const updateWardDataOrder = input => ({
  type: 'OrderReducer_UPDATE_WARD_DATA',
  data: input,
});

export const updateCurrentDistrictIdOrder = input => ({
  type: 'OrderReducer_UPDATE_CURRENT_DISTRICT_ID',
  data: input,
});

export const updateCurrentWardIdOrder = input => ({
  type: 'OrderReducer_UPDATE_CURRENT_WARD_ID',
  data: input,
});

export const updateRestaurantId = input => ({
  type: 'OrderReducer_UPDATE_RESTAURANT_ID',
  data: input,
});

export const updateLoadingOrder = input => ({
  type: 'OrderReducer_UPDATE_LOADING',
  data: input,
});

/*
    District And Ward Reducer actions
*/

export const resetDistrictAndWardReducer = () => ({
  type: 'DistrictAndWardReducer_RESET_DATA',
});

export const loadDistrictData = input => ({
  type: 'DistrictAndWardReducer_LOAD_DISTRICT_DATA',
  data: input,
});

export const selectDistrict = input => ({
  type: 'DistrictAndWardReducer_SELECT_DISTRICT',
  data: input,
});

export const getWardByDistrict = input => ({
  type: 'DistrictAndWardReducer_LOAD_WARD_BY_DISTRICT',
  data: input,
});

export const selectWard = input => ({
  type: 'DistrictAndWardReducer_SELECT_WARD',
  data: input,
});

export const updateStreet = input => ({
  type: 'DistrictAndWardReducer_UPDATE_STREET',
  data: input,
});

export const updatePhoneNumber = input => ({
  type: 'DistrictAndWardReducer_UPDATE_PHONENUMBER',
  data: input,
});

/*
    Merchants Location Reducer actions
*/

export const loadMerchantsLocation = input => ({
  type: 'MerchantsLocationReducer_LOAD_DATA',
  data: input,
});

/*
    Notification Reducer actions
*/

export const getNotification = input => ({
  type: 'NotificationReducer_LOAD_DATA',
  data: input,
});
